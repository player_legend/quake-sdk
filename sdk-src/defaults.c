/*
  Copyright (C) 2018  Joshua D. Sawyer

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "defaults.h"
#include "array.h"
#include "print.h"

struct config g_config;
#include <stdio.h>

#ifndef __SDK_DIR
#define __SDK_DIR "/opt/quake-sdk"
#endif

bool recursive_get_var( struct config_var * var )
{
  if( var == NULL )
    {
      return true;
    }

  if( var->value != NULL )
    {
      return true;
    }

  char * env = getenv( var->name );
  
  if( env != NULL )
    {
      var->value = text__new( env );
      return true;
    }
  
  if( var->parent != NULL )
    {
      if( !recursive_get_var( var->parent ) )
	{
	  return false;
	}

      var->value = text__new( var->parent->value );
    }
  else
    {
      var->value = text__new( NULL );
    }

  text__pathcat( &var->value, var->default_val );

  return true;
}

const char * get_var( struct config_var * var )
{
  if( !recursive_get_var( var ) )
    {
      print__message( LEVEL__ERROR, "Couldn't determine the value of '%s'\n", var->name );
      return NULL;
    }

  return var->value;
}

void assign_default_config( struct config * config )
{
  if( config->is_set == true )
    {
      return;
    }

  *config = (struct config)
    {
      .is_set = true,
      .user_home =
      {
	"HOME",
	NULL,
	"/root",
	NULL,
      },
      .dir =
      {
	"QUAKE_SDK__SDK_DIR",
	NULL,
	__SDK_DIR,
	NULL,
      },
      .maps_dir =
      {
	"QUAKE_SDK__MAPS_DIR",
	&config->dir,
	"map-source",
	NULL,
      },
      .mods_dir =
      {
	"QUAKE_SDK__QC_DIR",
	&config->dir,
	"mod-source",
	NULL,
      },
      .test_game =
      {
	"QUAKE_SDK__GAME_DIR",
	&config->dir,
	"test-game",
	NULL,
      },
      .qbsp_flags =
      {
	"QUAKE_SDK__QBSP_FLAGS",
	NULL,
	"",
	NULL,
      },
      .qcc_flags =
      {
	"QUAKE_SDK__QCC_FLAGS",
	NULL,
	"-Wall",
	NULL,
      },
      .bin =
      {
	"QUAKE_SDK__BIN_DIR",
	&config->dir,
	"bin",
	NULL,
      },
      .qcc_path =
      {
	"QUAKE_SDK__QCC_PATH",
	&config->bin,
#if __x86_64__
	"fteqcc64",
#else
	"fteqcc32",
#endif
	NULL,
      },
      .qbsp_path =
      {
	"QUAKE_SDK__QBSP_PATH",
	&config->bin,
	"qbsp",
	NULL,
      },
      .default_mod =
      {
	"QUAKE_SDK__DEFAULT_MOD",
	NULL,
	"id1",
	NULL,
      },
      .fte_path =
      {
	"QUAKE_SDK__FTE_PATH",
	&config->bin,
#if __x86_64__
	"fteqw64",
#else
	"fteqw32",
#endif
	NULL,
      },
      .qspasm_path =
      {
	"QUAKE_SDK__QSPASM_PATH",
	&config->bin,
	"quakespasm",
	NULL,
      },
      .default_map =
      {
	"QUAKE_SDK__DEFAULT_MAP",
	NULL,
	"start",
	NULL,
      },
      .text_editor_path =
      {
	"QUAKE_SDK__TEXT_EDITOR",
	NULL,
	"emacs",
	NULL,
      },
      .map_editor_path =
      {
	"QUAKE_SDK__MAP_EDITOR",
	NULL,
	"trenchbroom",
	NULL,
      },
      .light_path =
      {
	"QUAKE_SDK__LIGHT_PATH",
	&config->bin,
	"light",
	NULL,
      },
      .light_flags =
      {
	"QUAKE_SDK__LIGHT_FLAGS",
	NULL,
	"-bounce",
	NULL,
      },
      .local_server_port =
      {
	"QUAKE_SDK__LOCAL_SERVER_PORT",
	NULL,
	"27501",
	NULL,
      },
      .local_server_path =
      {
	"QUAKE_SDK__LOCAL_SERVER_PATH",
	&config->bin,
	"fteqw-sv32",
	NULL,
      },
      .remote_server_port =
      {
	"QUAKE_SDK__REMOTE_SERVER_PORT",
	NULL,
	"27501",
	NULL,
      },
      .remote_server_path =
      {
	"QUAKE_SDK__REMOTE_SERVER_PATH",
	&config->bin,
	"fteqw-sv32",
	NULL,
      },
      .remote_host =
      {
	"QUAKE_SDK__REMOTE_HOST",
	NULL,
	"localhost",
	NULL,
      },
      .remote_game_dir =
      {
	"QUAKE_SDK__REMOTE_GAME_DIR",
	&config->dir,
	"test-game",
	NULL,
      },
    };
}
