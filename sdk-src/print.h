/*
  Copyright (C) 2018  Joshua D. Sawyer

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
    print.h

    Functions for printing to standard output and to text buffers
*/
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef PRINT_H
#define PRINT_H

//==============================================================================
//    Includes
//==============================================================================

//
//    System
//

#include <stdlib.h>
#include <stdio.h>
char *strdup(const char *s);

//
//    Engine
//

#include "flags.h"
#include "term_colors.h"

//==============================================================================
//    Type definitions
//==============================================================================

//
//    print level flags
//

enum print_level {
  LEVEL__NORMAL   = FL_Null,
  LEVEL__VERBOSE  = FL_1,
  LEVEL__EXTRA    = FL_2,
  LEVEL__HEADER   = FL_3,
  LEVEL__ERROR    = FL_4,
  LEVEL__DEBUG    = FL_5,
};

//
//    Text buffer
//

struct text_buffer{
  char * text;
  size_t len;
  size_t alloc_len;
};

typedef struct text_buffer text_buffer_t;

#define NULL_text_buffer (text_buffer_t){}

//==============================================================================
//    Macros
//==============================================================================

#define BLANK_NULL_STRING( str ) (((str) == NULL)?(""):(str))
#define STRLEN(s) ((sizeof(s)/sizeof((s)[0])) - sizeof((s)[0]))
#define STRINGIZE_SUB(s) str(s)
#define STRINGIZE(s) #s

//==============================================================================
//    Output to stdio
//==============================================================================

int print__message( enum print_level level, const char * format, ... );
int print__startsection();
int print__endsection();
int print__query_section();

//==============================================================================
//    Output to a buffer
//==============================================================================

int  buffer__printf( text_buffer_t * buffer, const char * format, ... );
void buffer__clear( text_buffer_t * buffer );
void free_text_buffer_t( void * buffer );
int buffer__read_file( text_buffer_t * buffer, const char * file_name );

//==============================================================================

#endif // #ifndef PRINT_H
