/*
  Copyright (C) 2018  Joshua D. Sawyer

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "text.h"
#include "array.h"
#include "defaults.h"
#include "print.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
int strcasecmp(const char *s1, const char *s2);
char *strcasestr(const char *haystack, const char *needle);

#define EXTRA_SPACE 2

char * g_empty_str = "";

index_t current_section;

struct voidp_array text_section_list = {};
/*
static void free_section( void * tgt )
{
  voidp__clear( tgt, true );
}

static struct voidp_array * getsection()
{
  text_section_list.free = free_section;
  if( current_section >= array__COUNT( text_section_list ) )
    {
      voidp__set_index( &text_section_list, current_section, NULL, true );
    }

  if( array__ENTRY( text_section_list, current_section ).data == NULL )
    {
      struct voidp_array * new = malloc( sizeof( *new ) );
      *new = (struct voidp_array){};
      array__ENTRY( text_section_list, current_section ).data = new;
    }

  return array__ENTRY( text_section_list, current_section ).data;
}

void text__switchsection( index_t section )
{
  current_section = section;
}

index_t text__topsection()
{
  current_section = array__COUNT( text_section_list );
  if( current_section > 0 )
    {
      current_section -= 1;
    }

  return current_section;
}

index_t text__newsection()
{
  current_section = array__COUNT( text_section_list );
  getsection();
  
  return current_section;
}

void text__freesection( index_t section )
{
  text__switchsection( section );

  while( array__COUNT( text_section_list ) > section )
    {
      voidp__pop( &text_section_list, true );
    }

  if( section > 0 )
    {
      current_section = section - 1;
    }
  else
    {
      current_section = 0;
    }
}
*/

bool text__contains_fuzzy( const char * search, size_t ignore_len, const char * fuzzy )
{
  if( fuzzy == NULL )
    {
      return true;
    }

  size_t len = strlen( search );
  if( len <= ignore_len )
    {
      return false;
    }
  
  char dupe[len - ignore_len + 1];

  strcpy( dupe, search + ignore_len );

  char * extdot = strchr( dupe, '.' );
  if( extdot != NULL )
    {
      *extdot = '\0';
    }

  if( strcasestr( dupe, fuzzy ) != NULL )
    {
      return true;
    }
  else
    {
      return false;
    }
}

bool text__has_extension( const char * search, const char * extension )
{
  if( extension == NULL || extension[0] == '\0' )
    {
      return true;
    }
  
  size_t len = strlen( search );
  size_t exlen = strlen( extension );

  if( len < exlen )
    {
      return false;
    }

  if( strcasecmp( search + len - exlen, extension ) == 0 )
    {
      return true;
    }
  else
    {
      return false;
    }
}

char * text__new_sub( const char * start, const char * end )
{
  if( start == NULL )
    {
      start = g_empty_str;
    }
  
  if( end == NULL )
    {
      end = start + strlen( start );
    }

  char * ret = malloc( end - start + 1 + EXTRA_SPACE );
  strncpy( ret, start, end - start );
  ret[end - start] = '\0';

  voidp__push( &text_section_list, ret );

  return ret;
}

char * text__dirname( const char * path )
{
  char * end = strrchr( path, PATH_DELIM_CHAR );

  return text__new_sub( path, end );
}

char * text__new( const char * string )
{
  return text__new_sub( string, NULL );
}

char * text__itoa( int number )
{
  size_t len = snprintf( NULL, 0, "%d", number ) + 1;
  char text[len];
  snprintf( text, len, "%d", number );

  return text__new( text );
}

void text__cat( char ** dst, const char * src )
{
  size_t combine_len = strlen( *dst ) + strlen( src );

  if( combine_len == 0 )
    {
      return;
    }
  
  char * new_str = malloc( combine_len + 1 + EXTRA_SPACE );

  strcpy( new_str, *dst );
  strcat( new_str, src );

  if( *dst != src )
    {
      free( *dst );
    }
  
  *dst = new_str;
}

void text__delimcat( char ** dst, const char * src, char delim )
{
  size_t dst_len = strlen( *dst );

  if( dst_len > 0 && (*dst)[dst_len - 1] != delim )
    {
      (*dst)[dst_len] = delim;
      (*dst)[dst_len + 1] = '\0'; // exploiting the EXTRA_SPACE allocated
    }

  text__cat( dst, src );
}

void text__pathcat( char ** dst, const char * src )
{
  text__delimcat( dst, src, PATH_DELIM_CHAR );
}

void text__pathcat2( char ** dst, const char * src1, const char * src2 )
{
  text__pathcat( dst, src1 );
  text__pathcat( dst, src2 );
}

void text__wordcat( char ** dst, const char * src )
{
  text__delimcat( dst, src, ' ' );
}

char * text__get_name( const char * in_dir )
//    trim the directory and file extension off a string
{
  const char * start = strrchr( in_dir, '/' );

  if( start == NULL )
    {
      start = in_dir;
    }
  else
    {
      start = start + 1;
    }
  
  const char * end = strrchr( start, '.' );

  return text__new_sub( start, end );
}

char * text__get_target_name( struct voidp_array * list, struct config_var * default_var )
{
  char * name;
  
  if( array__COUNT( *list ) == 0 )
    {
      const char * default_text = get_var( default_var );
      if( default_text == NULL )
	{
	  return NULL;
	}
      else
	{
	  name = text__new( default_text );
	}
    }
  else
    {
      name = text__get_name( array__LAST_ENTRY( *list ).data );
    }

  return name;
}

bool text__user_confirm()
{
  while( 1 )
    {
      print__message( LEVEL__NORMAL, "[y/n]: " );
      switch( fgetc( stdin ) )
	{
	case 'y':
	  return true;

	case 'n':
	  return false;

	default:
	  print__message( LEVEL__ERROR, "Enter 'y' or 'n'\n" );
	}
    }

  return false;
}

char * text__path4( const char * one, const char * two, const char * three, const char * four, const char * extension )
{
  char * ret = text__new( one );
  text__pathcat( &ret, two );
  text__pathcat( &ret, three );
  text__pathcat( &ret, four );
  if( extension != NULL )
    {
      text__cat( &ret, extension );
    }
  return ret;
}

char * text__path3( const char * one, const char * two, const char * three, const char * extension )
{
  char * ret = text__new( one );
  text__pathcat( &ret, two );
  text__pathcat( &ret, three );
  if( extension != NULL )
    {
      text__cat( &ret, extension );
    }
  return ret;
}

char * text__path2( const char * one, const char * two, const char * extension )
{
  char * ret = text__new( one );
  text__pathcat( &ret, two );
  if( extension != NULL )
    {
      text__cat( &ret, extension );
    }
  return ret;
}

char * text__word2( const char * one, const char * two )
{
  char * ret = text__new( one );
  text__wordcat( &ret, two );

  return ret;
}

char * text__cut_dir( const char * dir, const char * cut )
{
  if( dir != NULL && strstr( dir, cut ) == dir )
    {
      return text__new( dir + strlen( cut ) );
    }
  else
    {
      return text__new( dir );
    }
}
