ERICW_TOOLS_REMOTE=https://github.com/ericwa/ericw-tools/releases/download/v0.18.1/ericw-tools-v0.18.1-Linux.zip
QCC32_REMOTE=http://fte.triptohell.info/moodles/linux_x86/fteqcc32
QCC64_REMOTE=http://fte.triptohell.info/moodles/linux_amd64/fteqcc64
FTEQW32_REMOTE=http://fte.triptohell.info/moodles/linux_x86/fteqw32
FTEQW64_REMOTE=http://fte.triptohell.info/moodles/linux_amd64/fteqw64
QUAKESPASM32_REMOTE=https://astuteinternet.dl.sourceforge.net/project/quakespasm/Linux/quakespasm-0.93.1_linux.tar.gz
QUAKESPASM64_REMOTE=https://managedway.dl.sourceforge.net/project/quakespasm/Linux/quakespasm-0.93.1_amd64.tar.gz

BIN_DIR=${BIN_DIR:-$(pwd)/../bin}
BUILD_DIR=${BUILD_DIR:-/tmp/quake-sdk-build}

echo
echo ==========================================
echo bin dir: $BIN_DIR
echo build dir: $BUILD_DIR
echo ==========================================
echo

mkdir -p "$BUILD_DIR"
mkdir -p "$BIN_DIR"

wget -c -N $ERICW_TOOLS_REMOTE -O $BUILD_DIR/ericw-tools.zip
#wget -c -N $QUAKESPASM32_REMOTE -O $BUILD_DIR/quakespasm32.zip # todo
#wget -c -N $QUAKESPASM64_REMOTE -O $BUILD_DIR/quakespasm64.zip
wget -c -N $QCC32_REMOTE -O $BIN_DIR/fteqcc32
wget -c -N $QCC64_REMOTE -O $BIN_DIR/fteqcc64
wget -c -N $FTEQW32_REMOTE -O $BIN_DIR/fteqw32
wget -c -N $FTEQW64_REMOTE -O $BIN_DIR/fteqw64

yes N | unzip $BUILD_DIR/ericw-tools.zip -d $BUILD_DIR
cp -rn $BUILD_DIR/ericw-tools-*/bin/* $BIN_DIR

echo

chmod +x $BIN_DIR/fteqcc32 $BIN_DIR/fteqcc64 $BIN_DIR/fteqw32 $BIN_DIR/fteqw64
