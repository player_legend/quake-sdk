/*
  Copyright (C) 2018  Joshua D. Sawyer

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "file.h"
#include "text.h"
#include "defaults.h"
#include "print.h"
#include "map.h"

#include <unistd.h>

void map__find( struct voidp_array * list, const char * fuzzy_filter )
{
  const char * map_extension = ".map";
  
  //file__list_var_subdirs( list, &g_config.maps_dir, fuzzy_filter, map_extension, false );

  const char * maps_dir = get_var( &g_config.maps_dir );

  if( maps_dir == NULL )
    {
      print__message( LEVEL__ERROR, "Couldn't search for maps." );
      return;
    }

  file__recursive_find_files( list, maps_dir, fuzzy_filter, map_extension );
}

bool map__compile( const char * map_source, const char * mod_name )
{
  const char
    * game_dir = get_var( &g_config.test_game ),
    * light_path = get_var( &g_config.light_path ),
    * light_flags = get_var( &g_config.light_flags ),
    * qbsp_path = get_var( &g_config.qbsp_path ),
    * qbsp_flags = get_var( &g_config.qbsp_flags );

  char
    * map_name = text__get_name( map_source ),
    * map_dest = NULL,
    * light_command = NULL,
    * qbsp_command = NULL;
  
  if( game_dir == NULL || qbsp_path == NULL || qbsp_flags == NULL || light_path == NULL || light_flags == NULL )
    {
      goto FAIL;
    }

  //    map_dest
  {
    map_dest = text__new( game_dir );
    text__pathcat( &map_dest, mod_name );
    text__pathcat( &map_dest, "maps" );
    file__recursive_mkdir( map_dest );
    chdir( map_dest );
    text__pathcat( &map_dest, (const char *)map_name );
    text__cat( &map_dest, ".bsp" );
  }

  //    qbsp_command
  {
    qbsp_command = text__new( qbsp_path );
    text__wordcat( &qbsp_command, qbsp_flags );
    text__wordcat( &qbsp_command, map_source );
    text__wordcat( &qbsp_command, map_dest );
  }

  //    light command
  {
    light_command = text__new( light_path );
    text__wordcat( &light_command, light_flags );
    text__wordcat( &light_command, map_dest );
  }
  
  if( !file__src_is_newer( map_source, map_dest ) )
    {
      print__message( LEVEL__NORMAL, "'%s' is up to date\n", map_name );
      goto SUCCESS;
    }

  printf("\n================================================================================\n");
  printf("\tCompile %s.bsp to %s\n", map_name, mod_name );
  printf("\tQBSP Command %s\n", qbsp_command);
  printf("\tLight Command %s\n", light_command);
  printf("================================================================================\n\n");
      
  fflush( stdout );
  
  if( system( qbsp_command ) != 0 )
    {
      print__message( LEVEL__ERROR, "qbsp returned nonzero '%s'\n", qbsp_command );
      goto FAIL;
    }
  
  if( system( light_command ) != 0 )
    {
      print__message( LEVEL__ERROR, "light returned nonzero '%s'\n", light_command );
      map__clear( map_source, mod_name );
      goto FAIL;
    }
  
  print__message( LEVEL__NORMAL, "Compiled map '%s'\n", map_name );

 SUCCESS:
		      
  free( map_name );
  free( map_dest );
  free( qbsp_command );
  free( light_command );
	
  fflush( stdout );
  
  return true;

 FAIL:
  print__message( LEVEL__ERROR, "Couldn't compile map '%s'\n", map_name );
  
  free( map_name );
  free( map_dest );
  free( qbsp_command );
  free( light_command );
  
  fflush( stdout );
  
  return false;
}

index_t map__compile_list( const struct voidp_array * list, char * mod_name )
{
  index_t compile_count = 0;
  index_t name_index;
  char * name;

  print__startsection();
  array__FOR_EACH( data, *list, name_index, name )
    {
      if( map__compile( name, mod_name ) )
	{
	  compile_count++;
	}
      else
	{
	  print__message( LEVEL__ERROR, "Compiling map '%s' failed\n", name );
	  break;
	}
    }
  print__endsection();

  return compile_count;
}

index_t map__guess_mod_and_compile_maps( struct voidp_array * map_list, struct voidp_array * mod_list )
{
  char * mod_name;
  {
    if( array__COUNT( *mod_list ) == 0 )
      {
	const char * default_mod = get_var( &g_config.default_mod );
	if( default_mod == NULL )
	  {
	    print__message( LEVEL__ERROR, "Couldn't compile '%lu' maps.\n", (unsigned long)array__COUNT( *map_list ) );
	    return false;
	  }
	mod_name = text__new( default_mod );
      }
    else
      {
	mod_name = text__get_name( array__LAST_ENTRY( *mod_list ).data );
      }
  }

  print__message( LEVEL__NORMAL, "Compiling maps to the mod '%s'\n", mod_name );
  index_t count = map__compile_list( map_list, mod_name );

  free( mod_name );
  
  return count;
}

char * map__get_target_name( struct voidp_array * map_list )
{
  return text__get_target_name( map_list, &g_config.default_map );
}

int map__edit( const char * map )
{
  const char * map_extension = ".map";
  
  const char * map_editor = get_var( &g_config.map_editor_path );

  if( map_editor == NULL )
    {
      print__message( LEVEL__ERROR, "Could not edit map '%s'\n", map );
      return -1;
    }
  printf("Edit %s\n", map);
  
  char * map_source = text__new( map );
  if( !text__has_extension( map_source, map_extension ) )
    {
      text__cat( &map_source, map_extension );
    }

  char * command = text__new( map_editor );
  text__wordcat( &command, map_source );

  return system( command );
}

void map__clear( const char * map, const char * mod_name )
{
  const char * game_dir = get_var( &g_config.test_game );
  
  if( game_dir == NULL )
    {
      return;
    }

  char * map_name = text__get_name( map );

  char * map_dest = text__path4( game_dir, mod_name, "maps", map_name, ".bsp" );

  file__remove( map_dest );

  free( map_name );
  free( map_dest );
}

void map__clear_list( struct voidp_array * map_list, const char * map_name )
{
  index_t index;
  const char * name;

  print__message( LEVEL__NORMAL, "Clearing %u maps\n", (unsigned int)array__COUNT( *map_list ) );
  print__startsection();
  array__FOR_EACH( data, *map_list, index, name )
    {
      map__clear( name, map_name );
    }
  print__endsection();
  printf("\n");
}

bool map__new( const char * map )
{
  const char * map_extension = ".map";
  const char * map_source_dir = get_var( &g_config.maps_dir );
  const char * map_editor = get_var( &g_config.map_editor_path );

  const char * new_map_content =
    "\
// Game: Quake \n// Format: Standard \n// entity 0 \n{ \n\"classname\" \"worldspawn\" \n// brush 0 \n{ \n( -64 -64 -16 ) ( -64 -63 -16 ) ( -64 -64 -15 ) __TB_empty 0 0 0 1 1 \n( 64 64 16 ) ( 64 64 17 ) ( 64 65 16 ) __TB_empty 0 0 0 1 1 \n( -64 -64 -16 ) ( -64 -64 -15 ) ( -63 -64 -16 ) __TB_empty 0 0 0 1 1 \n( 64 64 16 ) ( 65 64 16 ) ( 64 64 17 ) __TB_empty 0 0 0 1 1 \n( 64 64 16 ) ( 64 65 16 ) ( 65 64 16 ) __TB_empty 0 0 0 1 1 \n( -64 -64 -16 ) ( -63 -64 -16 ) ( -64 -63 -16 ) __TB_empty 0 0 0 1 1 \n}\n}";
  
  if( map_source_dir == NULL || map_editor == NULL )
    {
      return false;
    }
  
  char * map_path;
  char * map_dir;
  {
    const char * ext = text__has_extension( map, map_extension ) ? NULL : map_extension;
    if( strchr( map, PATH_DELIM_CHAR ) == NULL )
      {
	map_path = text__path3( map_source_dir, "misc", map, ext );
	map_dir = text__path2( map_source_dir, "misc", NULL );
      }
    else
      {
	map_path = text__path2( map_source_dir, map, ext );
	char * end = strrchr( map_path, PATH_DELIM_CHAR );
	map_dir = text__new_sub( map_path, end );
	
      }
  }

  file__recursive_mkdir( map_dir );
  
  if( !file__exists( map_path ) )
    {
      FILE * file = fopen( map_path, "w" );
      if( file != NULL )
	{
	  fwrite( new_map_content, strlen( new_map_content ), 1, file );
	  fclose( file );
	}
    }
  else
    {
      print__message( LEVEL__NORMAL, "Map exists '%s'\n", map_path );
    }

  map__edit( map_path );

  free( map_path );
  
  return true;
}

void map__print_path_list( struct voidp_array * list )
{
  index_t index;
  const char * path;

  const char * maps_dir = get_var( &g_config.maps_dir );
  const char * mods_dir = get_var( &g_config.mods_dir );

  char * cut1;
  char * cut2;

  print__startsection();

  array__FOR_EACH( data, *list, index, path )
    {
      cut1 = text__cut_dir( path, maps_dir );
      cut2 = text__cut_dir( cut1, mods_dir );

      print__message( LEVEL__NORMAL, "%s\n", cut2 );
      
      free( cut1 );
      free( cut2 );
    }
  
  print__endsection();
  printf("\n");
}
