/*
  Copyright (C) 2018  Joshua D. Sawyer

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef MOD_H
#define MOD_H

#include "array.h"

void mod__find( struct voidp_array * list, const char * fuzzy_filter );
  
char * mod__get_target_name( struct voidp_array * mod_list );
char * mod__get_mod_dest( const char * mod );
int mod__edit( const char * mod_dir );
bool mod__compile_qc( const char * mod_dir );
index_t mod__compile_list( struct voidp_array * mod_list );
void mod__clear( const char * mod );
void mod__clear_list( struct voidp_array * mod_list );
void mod__print_path_list( struct voidp_array * list );
bool mod__sync( const char * mod_name );

#endif // #ifndef MOD_H
