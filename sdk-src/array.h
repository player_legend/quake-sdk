/*
  Copyright (C) 2018  Joshua D. Sawyer

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef COMMON_ARRAY_H
#define COMMON_ARRAY_H

//==============================================================================
//    Includes
//==============================================================================

//
//    Engine
//

#include "flags.h"

//
//    System
//

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>

//==============================================================================
//    Type Definitions
//==============================================================================

//
//    general
//

struct size_buf{
  union {
    void * data;
    char * string;
  };
  size_t size;
};

typedef uintptr_t index_t;
typedef void (*free_f)( void * free_target ); // callback for free-like functions
typedef intptr_t type_id_t;

#define INVALID_INDEX UINTPTR_MAX

//
//    data array
//

struct data_array{
  index_t count;
  index_t alloc_count;
  void * entry;
  size_t entry_size;
};

#define data__COUNT( target )			\
  ((target).count)

#define data__ENTRY( target, typecast, index_var )	\
  ( ((typecast*)((target).entry))[index_var] )

#define data__FOR_EACH_ENTRY( target, typecast, index_var, entry_var )	\
  for( (index_var) = 0;							\
       ((index_var) < array__COUNT( target ))				\
         && ((entry_var) = &data__ENTRY( target, typecast, index_var ),	\
             1);							\
       (index_var)++ )

//
//    voidp array
//

struct voidp_entry{
  void * data;
};

struct voidp_array{
  index_t count;
  index_t alloc_count;
  struct voidp_entry * entry;
  free_f free;
};

#define VOIDP_DATA( data_var ) (struct voidp_entry){ .data = data_var }

//
//    hash table
//

typedef unsigned int hash_t;

struct hash_entry{
  char * name;
  hash_t hash;
};

struct hash_array{
  index_t count;
  index_t alloc_count;
  struct hash_entry * entry;
  bool index_ordering;
};

#define HASH_ENTRY( string_p, hash_var ) (struct hash_entry){ .name = string_p, .hash = hash_var }

//
//    type array
//

struct type_entry{
  free_f free;
  type_id_t id;
};

struct type_array{
  index_t count;
  index_t alloc_count;
  struct type_entry * entry;
};

#define TYPE_ENTRY( id_var, free_var ) (struct type_entry){ .id = id_var, .free = free_var }

//
//    arty: array with types
//

struct arty_entry{
  void * data;
  size_t size;
  type_id_t type_id;
};

struct arty_array{
  index_t count;
  index_t alloc_count;
  struct arty_entry * entry;
  struct type_array type_array;
};

#define ARTY_ENTRY( data_p, size_var, type_var ) (struct arty_entry){ .data = data_p, .size = size_var, .type_id = type_var }

//
//    cache
//

struct cache_entry{
  void * data;
  char * name;
  hash_t hash;
  size_t size;
  type_id_t type_id;
};

struct cache_array{
  index_t count;
  index_t alloc_count;
  struct cache_entry * entry;
  struct type_array type_array;
};

#define CACHE_ENTRY( string_p, hash_var, type_var, data_var, size_var ) (struct cache_entry){ .name = string_p, .hash = hash_var, .type_id = type_var, .data = data_var, .size = size_var }

//
//    Tree
//

enum tree_walk_target
  {
    WALK__PARENT_PRE,
    WALK__CHILD,
    WALK__PARENT_POST
  };

enum tree_return
  {
    TREE__CONT,
    TREE__STOP,
    TREE__NEXT_PEER,
    TREE__PARENT,
    TREE__SKIP,
    TREE__ISHIFT,
    TREE__REPEAT,
  };

enum tree_walk_flags
  {
    WALKFL__NO_PRE   = FL_1,
    WALKFL__NO_CHILD = FL_2,
    WALKFL__NO_POST  = FL_3,
    WALKFL__REVERSE  = FL_4,
  };

struct tree_array{
  //    array
  index_t count;
  index_t alloc_count;
  struct tree_array * entry;
  //    member
  void * data;
  type_id_t flags;
  type_id_t type_id;
  struct tree_array * parent;
};

struct tree_arg{
  struct tree_array * root;
  struct tree_array * target;
  index_t depth;
  index_t index;
  void * arg_data;
  enum tree_walk_target walk_target;
};

typedef enum tree_return (*tree_f)( struct tree_arg * arg );

#define INIT_tree_array( parent_p, data_p, type_var, flags_var ) (struct tree_array){ .parent = parent_p, .data = data_p, .type_id = type_var, .flags = flags_var }

//
//    Double linked list
//

struct list2_member{
  struct list2_member * previous;
  struct list2_member * next;
  struct list2 * parent;
  type_id_t type_id;
};

struct list2{
  struct list2_member member;
  struct list2_member * first;
  struct list2_member * last;
  struct voidp_array parking;
  struct type_array type_array;
  index_t count;
};

enum list2_return{
  LIST2__CONTINUE,
  LIST2__NEXT_PEER,
  LIST2__ASCEND,
  LIST2__STOP,
};

struct list2_arg{
  struct list2 * root;
  struct list2 * target;
  index_t depth;
  void * arg_data;
  enum tree_walk_target walk_target;
};

typedef enum list2_return (*list2_f)( struct list2_arg * arg );

#define list__FIRST( list )			\
  ((list).first)

#define list__LAST( list )			\
  ((list).last)

// calculates the next member before the cycle so it's safe to unlink the list_member in the loop
#define list2__FOR_EACH_LINK( list_parent, list_member )	\
  for( struct list2_member * internal_next = (void*)list__FIRST( list_parent ); \
       (NULL != ((list_member) = (void*)internal_next)) && (internal_next = internal_next->next, 1); \
       )

//==============================================================================
//    Generic manipulation macros
//==============================================================================

// allocating

#define array__ALLOC_COUNT( target )            \
  ((target).alloc_count)

#define array__ALLOC_NEXT( target )				\
  if( array__COUNT( target ) >= array__ALLOC_COUNT( target ) )		\
    {									\
      array__ALLOC_COUNT( target ) += array__COUNT( target ) + 10;	\
      (target).entry = realloc( (target).entry,                         \
                                array__ALLOC_COUNT( target )            \
                                * sizeof( array__FIRST_ENTRY( target ) ) ); \
    }
  

#define array__ALLOC_INDEX( target, index )	\
  if( (index) >= array__ALLOC_COUNT( target ) )                         \
    {                                                                   \
      array__ALLOC_COUNT( target ) += (index) + 5;			\
      (target).entry = realloc( (target).entry,                         \
                                array__ALLOC_COUNT( target )            \
                                * sizeof( array__FIRST_ENTRY( target ) ) ); \
    }

#define array__NULL_SPAN( target, index, null_member_initializer )	\
      for( index_t i = array__COUNT( target ); i <= (index); i++ )	\
	{								\
	  array__ENTRY( target, i ) = null_member_initializer;		\
	}

#define array__ADD_AFTER( target, index, null_member_initializer )	\
  array__ALLOC_INDEX( target, index );					\
  array__NULL_SPAN( target, index, null_member_initializer );		\
  array__COUNT( target ) = (index) + 1;

// push

#define array__PUSH( target, entry_var )		\
  array__ALLOC_NEXT( target );	\
  array__BOUND_ENTRY( target ) = entry_var;	\
  array__COUNT( target ) += 1;

#define array__IPUSH( target )                  \
  (array__COUNT( target )--)

// shift

#define array__SHIFT( target, index, entry_var, null_member_initializer ) \
  if( (index) < array__COUNT( target ) )				\
    {									\
      array__ALLOC_NEXT( target );					\
      memmove( (target).entry + (index) + 1,				\
	(target).entry + (index),					\
               sizeof( array__FIRST_ENTRY( target ) )			\
               * (array__COUNT( target ) - (index)) );			\
      array__COUNT( target ) += 1;					\
    }									\
  else									\
    {									\
      array__ADD_AFTER( target, index, null_member_initializer );	\
    }									\
  array__ENTRY( target, index ) = (entry_var);


#define array__ISHIFT( target, index )                  \
  memmove( (target).entry + (index),                     \
          (target).entry + (index) + 1,                 \
           sizeof( array__FIRST_ENTRY( target ) )       \
           * (array__COUNT( target ) - (index) - 1) );  \
  array__COUNT( target )--;


#define array__ISHIFT_RANGE( target, min, max )		 \
  memmove( (target).entry + (min),                     \
          (target).entry + (max) + 1,                 \
           sizeof( array__FIRST_ENTRY( target ) )       \
           * (array__COUNT( target ) - (max) - 1) );  \
  array__COUNT( target ) -= (max) - (min) + 1;

// flip

#define array__FLIP( target, index, entry_var, null_member_initializer ) \
  if( (index) < array__COUNT( target ) )				\
    {									\
      array__PUSH( target, array__ENTRY( target, index ) );		\
    }									\
  else									\
    {									\
      array__ADD_AFTER( target, index, null_member_initializer );	\
    }									\
  array__ENTRY( target, index ) = (entry_var);

#define array__IFLIP( target, index )                   \
  (array__ENTRY( target, index )                        \
   = array__ENTRY( target, --array__COUNT( target ) ))

//==============================================================================
//    General macros
//==============================================================================

//    accessing

#define array__COUNT( target )                  \
  ((target).count)

#define array__LAST_INDEX( target )		\
  ((target).count - 1)

#define array__ENTRY( target, index )           \
  ((target).entry[index])

#define array__FIRST_ENTRY( target )            \
  ((target).entry[0])

#define array__DATA( target, index, cast )	\
  ((cast)(array__ENTRY( target, index ).data))

#define array__LAST_DATA( target, cast )	\
  array__DATA( target, array__COUNT( target ) - 1, cast )

#define array__LAST_ENTRY( target )             \
  ((target).entry[array__LAST_INDEX( target )])

#define array__BOUND_ENTRY( target )                    \
  array__ENTRY( target, array__COUNT( target ) )

#define array__POP( target )			\
  array__ENTRY( target, --array__COUNT( target ) )

//    loops

#define array__FLUSH( target, entry_var, null_initializer )		\
  if( array__COUNT( target ) > 0 )                      \
    while( ( entry_var = &array__LAST_ENTRY( target ),   \
             array__COUNT( target )-- > 0) ||           \
	   ( array__COUNT( target ) = 0,		\
             0) )                                       \
      
#define array__FOR_EACH_ENTRY( target, index_var, entry_var )   \
  for( (index_var) = 0;                                         \
       ((index_var) < array__COUNT( target ))                   \
         && ((entry_var) = &array__ENTRY( target, index_var ),   \
             1);                                                \
       (index_var)++ )
 
#define array__REVERSE_FOR_EACH_ENTRY( target, index_var, entry_var )   \
  for( (index_var) = array__LAST_INDEX( target );			\
       ((index_var) < array__COUNT( target ))                   \
         && ((entry_var) = &array__ENTRY( target, index_var ),   \
             1);                                                \
       (index_var)-- )
 
#define array__FOR_EACH( entry_member, target, index_var, data_var )	\
  for( (index_var) = 0;							\
       ((index_var) < array__COUNT( target ))				\
	 && ((data_var) = array__ENTRY( target, index_var ).entry_member, \
             1);							\
       (index_var)++ )

#define array__FOR_EACH_DATA( target, index_var, data_var )		\
  for( (index_var) = 0;							\
       ((index_var) < array__COUNT( target ))				\
         && ((data_var) = array__ENTRY( target, index_var ).data,	\
             1);							\
       (index_var)++ )

#define array__REVERSE_FOR_EACH_DATA( target, index_var, data_var )		\
  for( (index_var) = array__LAST_INDEX( target );			\
       ((index_var) < array__COUNT( target ))				\
         && ((data_var) = array__ENTRY( target, index_var ).data,	\
             1);							\
       (index_var)-- )

//==============================================================================
//    Preprocessor switches
//==============================================================================

# define COMMON_ARRAY_VOIDP
# define COMMON_ARRAY_TYPE
	     //# define COMMON_ARRAY_ARTY
# define COMMON_ARRAY_HASH
# define COMMON_ARRAY_CACHE
# define COMMON_ARRAY_TARR
# define COMMON_ARRAY_LIST2
# define COMMON_ARRAY_LIST2TREE

//==============================================================================
//    voidp array
//==============================================================================

#define PTR_REALLOC( ptr, size )		\
  ptr = realloc( ptr, size );

#define PTR_REINDEX( ptr, index )		\
  PTR_REALLOC( ptr, (index)*sizeof( *ptr ) )
  
void data__push( struct data_array * target, void * data );
void data__alloc_index( struct data_array * target, index_t index );

# ifdef COMMON_ARRAY_VOIDP

void    voidp__push( struct voidp_array * target, void * data );
void    voidp__flip( struct voidp_array * target, index_t index, void * data );
void    voidp__shift( struct voidp_array * target, index_t index, void * data );
void    voidp__set_index( struct voidp_array * target, index_t index, void * data, bool free_data );
void *  voidp__pop( struct voidp_array * target, bool free_data );
void    voidp__iflip( struct voidp_array * target, index_t index, bool free_data );
void    voidp__ishift( struct voidp_array * target, index_t index, bool free_data );
index_t voidp__find( struct voidp_array * target, void * data );
void    voidp__clear( struct voidp_array * target, bool free_data );

# endif

# ifdef COMMON_ARRAY_TYPE

void type__add( struct type_array * target, type_id_t id, free_f free );
void type__free( struct type_array * target, type_id_t id, void * data );
void type__clear( struct type_array * target, bool free_data );

# endif

# ifdef COMMON_ARRAY_ARTY

void arty__push( struct arty_array * target, void * data, size_t size, type_id_t type );
void arty__flip( struct arty_array * target, index_t index, void * data, size_t size, type_id_t type );
void arty__shift( struct arty_array * target, index_t index, void * data, size_t size, type_id_t type );
void arty__ipush( struct arty_array * target, bool free_data );
void arty__iflip( struct arty_array * target, index_t index, bool free_data );
void arty__ishift( struct arty_array * target, index_t index, bool free_data );
void arty__set_index( struct arty_array * target, index_t index, void * data, size_t size, type_id_t type, bool free_data );
void arty__clear( struct arty_array * target, bool free_data );

# endif

# ifdef COMMON_ARRAY_HASH

index_t hash__find_name( struct hash_array * target, const char * name );
index_t hash__add_name( struct hash_array * target, const char * name );
index_t hash__push_name( struct hash_array * target, const char * name );
void    hash__clear( struct hash_array * target, bool free_data );

# endif

# ifdef COMMON_ARRAY_CACHE

void                 cache__add_type( struct cache_array * target, type_id_t id, free_f free );
index_t              cache__find_index( const struct cache_array * target, const char * name );
struct cache_entry * cache__find_entry( const struct cache_array * target, const char * name );
void *               cache__find_data( const struct cache_array * target, const char * name );
index_t              cache__add_name( struct cache_array * target, const char * name, void * data, size_t size, type_id_t type );
void                 cache__delete_name( struct cache_array * target, const char * name, bool free_data );
void                 cache__delete_index( struct cache_array * target, index_t index, bool free_data );
void                 cache__clear( struct cache_array * target, bool free_data );

# endif

# ifdef COMMON_ARRAY_TARR

struct tree_array * tarr__push( struct tree_array * target, void * data, type_id_t type, type_id_t flags );
struct tree_array * tarr__set_index( struct tree_array * target, index_t index, void * data, type_id_t type, type_id_t flags );
void                tarr__walk( struct tree_array * root, void * arg_data, tree_f op_func, enum tree_walk_flags walkfl );
void                tarr__ishift( struct tree_array * target, index_t index, struct type_array * type_array, bool free_data );
void                tarr__ishift_range( struct tree_array * target, index_t min, index_t max, struct type_array * type_array, bool free_data );
struct tree_array * tarr__pop( struct tree_array * target, struct type_array * type_array, bool free_data );
void                tarr__clear( struct tree_array * target, struct type_array * type_array, bool free_data );

# endif

# ifdef COMMON_ARRAY_LIST2

void   list2__unlink( void * target_member_p, bool free_target_member );
void   list2__park( struct list2 * target, void * target_member_p );
void * list2__get_type( struct list2 * target, type_id_t type, size_t type_size );
void   list2__append( struct list2 * target, type_id_t type, void * compatible_struct );
void   list2__prepend( struct list2 * target, type_id_t type, void * compatible_struct );
void   list2__add_before( struct list2 * target, void * target_member_p, type_id_t type, void * compatible_struct );
void   list2__add_after( struct list2 * target, void * target_member_p, type_id_t type, void * compatible_struct );
void   list2__clear( struct list2 * target, bool free_data );

# endif

//==============================================================================

#endif // #ifndef COMMON_ARRAY_H
