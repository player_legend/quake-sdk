/*
  Copyright (C) 2018  Joshua D. Sawyer

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "print.h"
#include "array.h"
#include "map.h"
#include "mod.h"
#include "defaults.h"
#include "file.h"
#include "game.h"
#include "arg.h"

int main( int argc, char * argv[] )
{
  assign_default_config( &g_config );

  return arg__run( argc, argv );
}
