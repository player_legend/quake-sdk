/*
  Copyright (C) 2018  Joshua D. Sawyer

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "print.h"
#include "array.h"
#include "map.h"
#include "mod.h"
#include "defaults.h"
#include "game.h"
#include "arg.h"
#include "help.h"

#include <stdio.h>
#include <getopt.h>
#include <string.h>
char *strcasestr(const char *haystack, const char *needle);

enum engine_id string_to_engine( const char * string )
{
  if( strcasestr( string, "cl" ) )
    {
      if( strcasestr( string, "r" ) )
	{
	  return ENGINE__REMOTE_CLIENT;
	}
      else
	{
	  return ENGINE__LOCAL_CLIENT;
	}
    }
  if( strcasestr( string, "serv" ) || strcasestr( string, "sv" ) )
    {
      if( strcasestr( string, "rs" ) || strcasestr( string, "re" ) )
	{
	  return ENGINE__REMOTE_SERVER;
	}
      else
	{
	  return ENGINE__LOCAL_SERVER;
	}
    }
  
  if( strcasestr( string, "fte" ) || strcasestr( string, "world" ) || strcasestr( string, "qw" ) )
    {
      return ENGINE__FTE;
    }

  if( strcasestr( string, "spasm" ) )
    {
      return ENGINE__QUAKESPASM;
    }

  print__message( LEVEL__ERROR, "Unrecognized engine '%s', using default\n", string );

  return ENGINE__DEFAULT;
}

enum gamemode_id string_to_gamemode( const char * string )
{ 
  if( strcasestr( string, "single" ) || strcasestr( string, "sp" ) )
    {
      return GAMEMODE__SP;
    }
  
  if( strcasestr( string, "coop" ) )
    {
      return GAMEMODE__COOP;
    }
  
  if( strcasestr( string, "tdm" ) || strcasestr( string, "team" ) )
    {
      return GAMEMODE__TDM;
    }
  
  if( strcasestr( string, "dm" ) || strcasestr( string, "deathmatch" ) )
    {
      if( strcasestr( string, "1" ) )
	{
	  return GAMEMODE__DM1;
	}
      if( strcasestr( string, "2" ) )
	{
	  return GAMEMODE__DM2;
	}
      if( strcasestr( string, "3" ) )
	{
	  return GAMEMODE__DM3;
	}
      if( strcasestr( string, "4" ) )
	{
	  return GAMEMODE__DM4;
	}

      return GAMEMODE__DM1;
    }

  print__message( LEVEL__ERROR, "Unrecognized gamemode '%s', using default\n", string );

  return GAMEMODE__DEFAULT;
}

int arg__run( int argc, char * argv[] )
{
  int ret = 0;
  
  int c;
  int option_index;

  bool no_compile = false;
  bool list_maps = false;
  bool list_mods = false;
  bool edit_targets = false;
  bool clear_targets = false;

  struct hash_array map_filters = {};
  struct hash_array mod_filters = {};

  static const struct option long_options[] =
    {
      { "map", required_argument, NULL, OPT__MAP },
      { "mod", required_argument, NULL, OPT__MOD },
      { "no-compile", no_argument, NULL, OPT__NO_COMPILE },
      { "list-maps", no_argument, NULL, OPT__LIST_MAPS },
      { "list-mods", no_argument, NULL, OPT__LIST_MODS },
      { "run-engine", required_argument, NULL, OPT__RUN_ENGINE },
      { "use-mode", required_argument, NULL, OPT__USE_MODE },
      { "splitscreen", required_argument, NULL, OPT__SPLITSCREEN },
      { "edit", no_argument, NULL, OPT__EDIT },
      { "clear", no_argument, NULL, OPT__CLEAR },
      { "new-map", required_argument, NULL, OPT__NEW_MAP },
    };

  bool run_engine = false;
  enum engine_id engine_id = ENGINE__DEFAULT;
  enum gamemode_id gamemode_id = GAMEMODE__DEFAULT;
  int screen_splits = 0;
  
  while( (c = getopt_long( argc, argv, "N:O:hceLlnm:M:r:s:", long_options, &option_index )) != -1 )
    {
      switch( c )
	{
	default:
	  print__message( LEVEL__ERROR, "Bad option %c -- %d\n", c, c );
	  break;

	case OPT__HELP:
	  help__text();
	  return 0;
	  
	case OPT__MAP:
	  hash__add_name( &map_filters, optarg );
	  break;

	case OPT__MOD:
	  hash__add_name( &mod_filters, optarg );
	  break;
	  
	case OPT__NO_COMPILE:
	  no_compile = true;
	  break;

	case OPT__LIST_MAPS:
	  list_maps = true;
	  no_compile = true;
	  break;

	case OPT__LIST_MODS:
	  list_mods = true;
	  no_compile = true;
	  break;

	case OPT__RUN_ENGINE:
	  run_engine = true;
	  engine_id = string_to_engine( optarg );
	  break;

	case OPT__USE_MODE:
	  run_engine = true;
	  gamemode_id = string_to_gamemode( optarg );
	  break;

	case OPT__SPLITSCREEN:
	  run_engine = true;
	  screen_splits = atoi( optarg );
	  break;

	case OPT__EDIT:
	  no_compile = true;
	  edit_targets = true;
	  break;

	case OPT__NEW_MAP:
	  map__new( optarg );
	  break;

	case OPT__CLEAR:
	  clear_targets = true;
	  no_compile = true;
	  break;
	}
    }
  
  struct voidp_array map_list = {};
  struct voidp_array mod_list = {};
  
  index_t filter_index;
  const char * filter;

  if( list_maps )
    {
      map__find( &map_list, NULL );
    }
  else
    {
      array__FOR_EACH( name, map_filters, filter_index, filter )
	{
	  map__find( &map_list, filter );
	}
    }

  if( list_mods )
    {
      mod__find( &mod_list, NULL );
    }
  else
    {
      array__FOR_EACH( name, mod_filters, filter_index, filter )
	{
	  mod__find( &mod_list, filter );
	}
    }

  char * mod_name = mod__get_target_name( &mod_list );
  
  index_t name_index;
  const char * path;
	  
  if( !ret && array__COUNT( map_list ) > 0 )
    {
      print__message( LEVEL__NORMAL, "Maps:\n" );
      
      if( no_compile )
	{
	  map__print_path_list( &map_list );
	}
      else
	{
	  index_t map_compile_count = map__compile_list( &map_list, mod_name );
	  print__message( LEVEL__NORMAL, "\nProcessed %u/%u maps\n", (unsigned long)map_compile_count, (unsigned long)array__COUNT( map_list ) );
	  if( map_compile_count != array__COUNT( map_list ) )
	    {
	      run_engine = false;
	      ret = 1;
	    }
	}

      if( clear_targets )
	{
	  map__clear_list( &map_list, mod_name );
	}
      
      if( edit_targets && array__COUNT( map_list ) > 5 )
	{
	  print__message( LEVEL__NORMAL, "Really edit %u maps?", (unsigned int)array__COUNT( map_list ) );
	  edit_targets = text__user_confirm();
	}
      
      if( edit_targets )
	{
	  array__FOR_EACH( data, map_list, name_index, path )
	    {
	      map__edit( path );
	    }
	}
    }

  if( !ret && array__COUNT( mod_list ) > 0 )
    {
      print__message( LEVEL__NORMAL, "Mods:\n" );

      if( no_compile )
	{
	  mod__print_path_list( &mod_list );
	}
      else
	{
	  index_t mod_compile_count = mod__compile_list( &mod_list );
	  print__message( LEVEL__NORMAL, "\nProcessed %u/%u mods\n", (unsigned long)mod_compile_count, (unsigned long)array__COUNT( mod_list ) );
	  if( mod_compile_count != array__COUNT( mod_list ) )
	    {
	      run_engine = false;
	      ret = 1;
	    }
	}
      
      if( edit_targets && array__COUNT( map_list ) > 5 )
	{
	  print__message( LEVEL__NORMAL, "Really edit %u mods?", (unsigned int)array__COUNT( map_list ) );
	  edit_targets = text__user_confirm();
	}
      
      if( clear_targets )
	{
	  mod__clear_list( &mod_list );
	}
      
      if( edit_targets )
	{
	  array__FOR_EACH( data, mod_list, name_index, path )
	    {
	      mod__edit( path );
	    }
	}
    }

  if( !ret && run_engine )
    {
      char * map_name = map__get_target_name( &map_list );
      char * mod_name = mod__get_target_name( &mod_list );

      if( map_name == NULL || mod_name == NULL )
	{
	  free( map_name );
	  free( mod_name );
	}
      
      ret = game__start( engine_id, gamemode_id, map_name, mod_name, screen_splits );
    }

  return ret;
}
