/*
  Copyright (C) 2018  Joshua D. Sawyer

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef ARG_H
#define ARG_H

#include <getopt.h>
#include <stdio.h>

enum cmd_options
  {
    OPT__HELP = 'h',
    OPT__MAP = 'm',
    OPT__MOD = 'M',
    OPT__NO_COMPILE = 'n',
    OPT__LIST_MAPS = 'l',
    OPT__LIST_MODS = 'L',
    OPT__SPLITSCREEN = 's',
    OPT__RUN_ENGINE = 'r',
    OPT__EDIT = 'e',
    OPT__CLEAR = 'c',
    OPT__USE_MODE = 'O',
    OPT__NEW_MAP = 'N',
  };

static const struct option long_options[] =
  {
    { "help", no_argument, NULL, OPT__HELP },
    { "map", required_argument, NULL, OPT__MAP },
    { "mod", required_argument, NULL, OPT__MOD },
    { "no-compile", no_argument, NULL, OPT__NO_COMPILE },
    { "list-maps", no_argument, NULL, OPT__LIST_MAPS },
    { "list-mods", no_argument, NULL, OPT__LIST_MODS },
    { "run-engine", required_argument, NULL, OPT__RUN_ENGINE },
    { "use-mode", required_argument, NULL, OPT__USE_MODE },
    { "splitscreen", required_argument, NULL, OPT__SPLITSCREEN },
    { "edit", no_argument, NULL, OPT__EDIT },
    { "clear", no_argument, NULL, OPT__CLEAR },
    { "new-map", required_argument, NULL, OPT__NEW_MAP },
  };

int arg__run( int argc, char * argv[] );

#endif // #ifdef ARG_H
