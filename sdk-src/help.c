/*
  Copyright (C) 2018  Joshua D. Sawyer

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "defaults.h"
#include "print.h"
#include "help.h"

#define RET "\n\t"
#define RET2 "\n\n"

#define HELP__HELP "-h --help" RET "print this help text"
#define HELP__MAP "-m --map" RET "Fuzzy filter for maps"
#define HELP__MOD "-M --mod" RET "Fuzzy filter for mods"
#define HELP__NO_COMPILE "-n --no-compile" RET "Do not compile the found maps/mods"
#define HELP__LIST_MAPS "-l --list-maps" RET "List all maps"
#define HELP__LIST_MODS "-L --list-mods" RET "List all mods"
#define HELP__SPLITSCREEN "-s --splitscreen" RET "Run FTEQW with a given number of extra, splitscreen clients"
#define HELP__RUN_ENGINE "-r --run-engine" RET "Specify an engine to run" RET "Valid are: fte, quakespasm, rserv, rcli, serv, cli" RET "rserv/rcli/serv/cli run a server/client on either a remote host or localhost"
#define HELP__EDIT "-e --edit" RET "Edit the found maps/mods"
#define HELP__CLEAR "-c --clear" RET "Clear compiled map/mod files, so that they can be recompiled from scratch"
#define HELP__USE_MODE "-O --use-mode" RET "Specify a gamemode to launch the engine with. Currently can do sp, coop, dm1, dm2, dm3, dm4 and tdm"
#define HELP__NEW_MAP "-N --new-map" RET "Specify a name for a new map, optionally with leading directories to be created under the map sources directory" RET "Names that do not contain a leading directory will be put in the 'misc' directory"

#define HELP_TEXT				\
  HELP__HELP RET2				\
  HELP__MAP RET2				\
  HELP__MOD RET2				\
  HELP__NEW_MAP RET2				\
  HELP__NO_COMPILE RET2				\
  HELP__LIST_MODS RET2				\
  HELP__LIST_MAPS RET2				\
  HELP__SPLITSCREEN RET2			\
  HELP__RUN_ENGINE RET2				\
  HELP__EDIT RET2				\
  HELP__CLEAR RET2				\
  HELP__USE_MODE				\
  
void help__print_var( struct config_var * var )
{
  const char * var_text = get_var( var );
  print__message( LEVEL__NORMAL, "%-30s= %s\n", var->name, var_text );
}

void help__vars()
{
  print__message( LEVEL__NORMAL, "Environment variables:\n" );

  print__startsection();
  
  help__print_var( &g_config.dir );
  help__print_var( &g_config.bin );
  help__print_var( &g_config.default_mod );
  help__print_var( &g_config.default_map );
  help__print_var( &g_config.qcc_path );
  help__print_var( &g_config.qcc_flags );
  help__print_var( &g_config.qbsp_path );
  help__print_var( &g_config.light_path );
  help__print_var( &g_config.light_flags );
  help__print_var( &g_config.maps_dir );
  help__print_var( &g_config.mods_dir );
  help__print_var( &g_config.fte_path );
  help__print_var( &g_config.qspasm_path );
  help__print_var( &g_config.map_editor_path );
  help__print_var( &g_config.text_editor_path );
  help__print_var( &g_config.test_game );
  help__print_var( &g_config.local_server_port );
  help__print_var( &g_config.local_server_path );
  help__print_var( &g_config.remote_server_port );
  help__print_var( &g_config.remote_server_path );
  help__print_var( &g_config.remote_host );
  help__print_var( &g_config.remote_game_dir );

  print__endsection();
}

void help__options()
{
  printf( "%s\n", HELP_TEXT );
  printf("\n");
}

void help__text()
{
  help__options();
  help__vars();
}
