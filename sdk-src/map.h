/*
  Copyright (C) 2018  Joshua D. Sawyer

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "array.h"

#include <stdbool.h>

void map__find( struct voidp_array * list, const char * fuzzy_filter );
bool map__compile( const char * map_source, const char * mod_name );
index_t map__compile_list( const struct voidp_array * list, char * mod_name );
index_t map__guess_mod_and_compile_maps( struct voidp_array * map_list, struct voidp_array * mod_list );
char * map__get_target_name( struct voidp_array * map_list );
int map__edit( const char * map );
void map__clear( const char * map, const char * mod_name );
void map__clear_list( struct voidp_array * map_list, const char * mod_name );
bool map__new( const char * map );
void map__clear( const char * map, const char * mod_name );
void map__print_path_list( struct voidp_array * list );
