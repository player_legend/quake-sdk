/*
  Copyright (C) 2018  Joshua D. Sawyer

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef TEXT_H
#define TEXT_H

#include <stdbool.h>
#include "array.h"

struct config_var;
struct voidp_array;

#define PATH_DELIM_CHAR '/'

bool text__contains_fuzzy( const char * search, size_t ignore_len, const char * fuzzy );
bool text__has_extension( const char * search, const char * extension );
char * text__new_sub( const char * start, const char * end );
char * text__new( const char * string );
void text__cat( char ** dst, const char * src );
void text__delimcat( char ** dst, const char * src, char delim );
void text__pathcat( char ** dst, const char * src );
void text__pathcat2( char ** dst, const char * src1, const char * src2 );
void text__wordcat( char ** dst, const char * src );
char * text__get_name( const char * in_dir );
char * text__dirname( const char * path );
char * text__itoa( int number );
char * text__get_target_name( struct voidp_array * list, struct config_var * default_var );
bool text__user_confirm();
char * text__path4( const char * one, const char * two, const char * three, const char * four, const char * extension );
char * text__path3( const char * one, const char * two, const char * three, const char * extension );
char * text__path2( const char * one, const char * two, const char * extension );
char * text__word2( const char * one, const char * two );
char * text__cut_dir( const char * dir, const char * cut );

#endif // #define TEXT_H
