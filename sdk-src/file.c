/*
  Copyright (C) 2018  Joshua D. Sawyer

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "file.h"
#include "print.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>

int stat(const char *pathname, struct stat *buf);
int access(const char *pathname, int mode);

time_t file__mod_time( const char * file_path )
{
  struct stat s;

  if( stat( file_path, &s ) < 0 )
    {
      return 0;
    }
  
  return s.st_mtime;
}

bool file__src_is_newer( const char * src, const char * dst )
{
  time_t
    srcmod = file__mod_time( src ),
    dstmod = file__mod_time( dst );

  return srcmod > dstmod;
}

bool file__src_list_is_newer( struct voidp_array * src_list, const char * dst )
{
  time_t dstmod = file__mod_time( dst );

  index_t src_index;
  const char * src_path;

  array__FOR_EACH( data, *src_list, src_index, src_path )
    {
      if( dstmod < file__mod_time( src_path ) )
	{
	  return true;
	}
    }

  return false;
}

bool file__src_list_dst_list_compare( struct voidp_array * src_list, struct voidp_array * dst_list )
{
  index_t dst_index;
  const char * dst_name;

  if( array__COUNT( *dst_list ) == 0 )
    {
      return true;
    }

  array__FOR_EACH( data, *dst_list, dst_index, dst_name )
    {
      if( file__src_list_is_newer( src_list, dst_name ) )
	{
	  return true;
	}
    }

  return false;
}

bool file__is_file( const char * path )
{
  struct stat s;
  stat( path, &s );
  return S_ISREG( s.st_mode );
}

bool file__is_dir( const char * path )
{
  struct stat s;
  stat( path, &s );
  return S_ISDIR( s.st_mode );
}

void file__list_dir( struct voidp_array * dir_list, const char * dir_name, const char * fuzzy_filter, size_t fuzzy_ignore, const char * extension, bool get_dirs )
{
  DIR * dir;
  struct dirent * ent;     

  char * found_path;
  
  dir = opendir( dir_name );

  if( dir == NULL )
    {
      print__message( LEVEL__ERROR, "Failed to open the directory '%s'\n", dir_name );
      return;
    }

  while( (ent = readdir( dir )) != NULL )
    {
      if( ent->d_name[0] == '.' )
	{
	  if( ent->d_name[1] == '\0' || (ent->d_name[1] == '.' && ent->d_name[2] == '\0') )
	    {
	      continue;
	    }
	}

      if( !text__has_extension( ent->d_name, extension ) )
	{
	  continue;
	}

      found_path = text__path2( dir_name, ent->d_name, NULL );
      
      if( !text__contains_fuzzy( found_path, fuzzy_ignore, fuzzy_filter ) )
	{
	  free( found_path );
	  continue;
	}
      
      if( ( get_dirs && !file__is_dir( found_path )) ||
	  (!get_dirs && !file__is_file( found_path )) )
	{
	  free( found_path );
	  continue;
	}

      voidp__push( dir_list, found_path );
    }

  closedir( dir );
}

void file__list_subdirs( struct voidp_array * list, const char * dir, const char * fuzzy_filter, size_t fuzzy_ignore, const char * extension, bool get_dirs )
{
  struct voidp_array dir_list = {};

  file__list_dir( &dir_list, dir, NULL, 0, NULL, true );

  index_t i;
  const char * name;

  array__FOR_EACH( data, dir_list, i, name )
    {
      file__list_dir( list, name, fuzzy_filter, fuzzy_ignore, extension, get_dirs );
    }

  voidp__clear( &dir_list, true );
}
  
/*void file__list_var_subdirs( struct voidp_array * list, struct config_var * var, const char * fuzzy_filter, size_t fuzzy_ignore, const char * extension, bool get_dirs )
{
  const char * dir = get_var( var );

  if( dir == NULL )
    {
      return;
    }
  
  file__list_subdirs( list, dir, fuzzy_filter, extension, get_dirs );
  }*/

void file__recursive_find_dirs( struct voidp_array * dir_list, const char * dir, const char * fuzzy_filter )
{
  voidp__push( dir_list, (void*)dir );

  index_t dir_index;
  const char * search_dir;

  array__FOR_EACH( data, *dir_list, dir_index, search_dir )
    {
      file__list_dir( dir_list, search_dir, fuzzy_filter, strlen( dir ), NULL, true );
    }
}

void file__recursive_find_files( struct voidp_array * file_list, const char * dir, const char * fuzzy_filter, const char * extension )
{
  struct voidp_array dir_list = {};
  file__recursive_find_dirs( &dir_list, dir, NULL );

  index_t dir_index;
  const char * search_dir;
  
  array__FOR_EACH( data, dir_list, dir_index, search_dir )
    {
      file__list_dir( file_list, search_dir, fuzzy_filter, strlen( dir ), extension, false );
    }

  array__ENTRY( dir_list, 0 ).data = NULL; // save the initial 'dir' entry

  voidp__clear( &dir_list, true );
}

bool file__exists( const char * path )
{
  if( access( path, F_OK ) == 0 )
    {
      return true;
    }
  else
    {
      return false;
    }
}
 
int file__recursive_mkdir( const char * target )
{
  if( file__exists( target ) )
    {
      return 0;
    }
  
  char * dupe = (char*)target; // hack: we'll put it back ... there's no read only memory in this program

  mode_t mode = 0755;
  size_t len = strlen( dupe );

  int ret = 0;

  for( size_t i = 0; i <= len; i++ )
    {
      if( dupe[i] == '/' )
	{
	  dupe[i] = '\0';
	  ret = mkdir( dupe, mode );
	  dupe[i] = '/';
	}
    }

  if( dupe[len] != '/' )
    {
      ret = mkdir( dupe, mode );
    }

  return ret;
}

void file__remove( const char * path )
{
  if( !file__exists( path ) )
    {
      return;
    }
  
  char * command = text__word2( "rm -rf", path );

  print__message( LEVEL__NORMAL, "Delete %s\n", path );

  system( command );

  free( command );
}

int file__sync_remote( const char * local_path, const char * host, const char * remote_path )
{
  text_buffer_t rsync_command = {};
  text_buffer_t mkdir_command = {};
  const char * rsync_flags = "-avruK --preallocate --delete";
  const char * rsync_program = "rsync";

  buffer__printf( &mkdir_command, "ssh %s \"mkdir -p %s\"", host, remote_path );
  buffer__printf( &rsync_command, "%s %s %s/ %s:%s/", rsync_program, rsync_flags, local_path, host, remote_path );

  printf("MKDIR: %s\n", mkdir_command.text );
  printf("RSYNC: %s\n", rsync_command.text );
  system( mkdir_command.text );
  int ret = system( rsync_command.text );

  buffer__clear( &mkdir_command );
  buffer__clear( &rsync_command );

  return ret;
}
