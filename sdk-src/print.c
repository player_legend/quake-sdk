/*
  Copyright (C) 2018  Joshua D. Sawyer

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
    print.c

    Functions for printing to standard output and to text buffers
*/
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


//==============================================================================
//    Includes
//==============================================================================

//
//    System
//

#include <stdarg.h>
#include <stdio.h>
#include <string.h>

//
//    Engine
//

#include "print.h"

//==============================================================================
//    Globals
//==============================================================================

int section_level; // the level for stdio print output

//==============================================================================
//    stdio print
//==============================================================================

int print__message( enum print_level level, const char * format, ... )
{
  int count;
  char insert_tab[section_level + 1];
  memset( insert_tab, '\t', section_level );
  insert_tab[section_level] = '\0';
  char mod_format[strlen(format) + 2*KCODE_LEN];

  char * color;

  if( level & LEVEL__ERROR )
    {
      color = KRED;
    }
  else if( level & LEVEL__EXTRA )
    {
      color = KYEL;
    }
  else if( level & LEVEL__HEADER )
    {
      color = KCYN;
    }
  else
    {
      color = NULL;
    }
  
  sprintf( mod_format, "%s%s%s", color == NULL ? "" : color, format, color == NULL ? "" : KNRM );
  printf("%s", insert_tab);
  
  va_list list;
  va_start( list, format );
  count = vprintf( mod_format, list );
  va_end( list );

  return count;
}

int print__startsection()
{
  section_level += 1;
  return section_level;
}

int print__endsection()
{
  section_level -= 1;
  
  if( section_level < 0 )
    {
      section_level = 0;
    }

  return section_level;
}

int print__query_section()
{
  return section_level;
}

//==============================================================================
//    buffer print
//==============================================================================

int buffer__printf( text_buffer_t * buffer, const char * format, ... )
{
  va_list list;
  va_start( list, format );

  size_t print_len = vsnprintf( NULL, 0, format, list );

  va_end( list );
  
  if( buffer->len + print_len >= buffer->alloc_len )
    {
      buffer->alloc_len += buffer->len + print_len + 100;
      
      buffer->text = realloc( buffer->text, buffer->alloc_len );

      if( buffer->text == NULL )
	{
	  printf("memory allocation error in buffer_printf\n");
	}
    }

  va_start( list, format );
  vsprintf( buffer->text + buffer->len, format, list );
  va_end( list );
  
  buffer->len += print_len;

  return print_len;
}

int buffer__read_file( text_buffer_t * buffer, const char * file_name )
{
  FILE * fp = fopen( file_name, "r" );
  size_t start_len = buffer->len;
  size_t add_len = 0;
  size_t red_len; // read len
  const size_t inc_len = 8192;
  
  if( fp != NULL )
    {
      do
	{
	  buffer->text = realloc( buffer->text,
				  start_len + add_len + inc_len );
	  
	  red_len = fread( buffer->text + start_len + add_len,
			   1,
			   inc_len,
			   fp );

	  add_len += red_len;
	}
      while( red_len == inc_len );


      buffer->alloc_len = buffer->len + add_len + 1;

      buffer->text = realloc( buffer->text, buffer->alloc_len );

      buffer->text[buffer->alloc_len - 1] = '\0';

      if( buffer->alloc_len > 0 )
	{
	  buffer->len = strlen( buffer->text );
	}

      fclose( fp );
      
      return add_len;
    }
  else
    {
      return 0;
    }
}

void buffer__clear( text_buffer_t * buffer )
{
  free( buffer->text );
  *buffer = NULL_text_buffer;
}

void free_text_buffer_t( void * buffer )
{
  free( ((text_buffer_t*)(buffer))->text );
  free( buffer );
}

//==============================================================================
