/*
  Copyright (C) 2018  Joshua D. Sawyer

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

enum engine_id
  {
    ENGINE__FTE,
    ENGINE__QUAKESPASM,
    ENGINE__LOCAL_SERVER,
    ENGINE__REMOTE_SERVER,
    ENGINE__LOCAL_CLIENT,
    ENGINE__REMOTE_CLIENT,
    ENGINE__DEFAULT = ENGINE__FTE,
  };

enum gamemode_id
  {
    GAMEMODE__SP,
    GAMEMODE__DM1,
    GAMEMODE__DM2,
    GAMEMODE__DM3,
    GAMEMODE__DM4,
    GAMEMODE__COOP,
    GAMEMODE__TDM,

    GAMEMODE__DEFAULT = GAMEMODE__SP,
  };

int game__start( enum engine_id engine_id, enum gamemode_id gamemode_id, const char * map, const char * mod, int screen_splits );
