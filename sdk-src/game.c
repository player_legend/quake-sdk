/*
  Copyright (C) 2018  Joshua D. Sawyer

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "print.h"
#include "game.h"
#include "text.h"
#include "defaults.h"
#include "mod.h"

#include <unistd.h>

const char * get_gamemode_arg( enum gamemode_id gamemode_id )
{
  static const char dm1_arg[] = "+deathmatch 1";
  static const char dm2_arg[] = "+deathmatch 2";
  static const char dm3_arg[] = "+deathmatch 3";
  static const char dm4_arg[] = "+deathmatch 4";
  static const char coop_arg[] = "+coop 1 +teamplay 1";
  static const char tdm_arg[] = "+deathmatch 1 +teamplay 1";
  static const char singleplayer_arg[] = "";

  switch( gamemode_id )
    {
    default:
      print__message( LEVEL__ERROR, "Unimplemented gamemode, using singleplayer instead" );
      
    case GAMEMODE__SP:
      return singleplayer_arg;
      
    case GAMEMODE__DM1:
      return dm1_arg;
            
    case GAMEMODE__DM2:
      return dm2_arg;
            
    case GAMEMODE__DM3:
      return dm3_arg;
            
    case GAMEMODE__DM4:
      return dm4_arg;
      
    case GAMEMODE__COOP:
      return coop_arg;
      
    case GAMEMODE__TDM:
      return tdm_arg;
    }
}

int game__start( enum engine_id engine_id, enum gamemode_id gamemode_id, const char * map, const char * mod, int screen_splits )
{
  const int maxclients = 16;

  char * mod_name = text__get_name( mod );
  char * map_name = text__get_name( map );

  if( engine_id == ENGINE__REMOTE_SERVER )
    {
      if( !mod__sync( mod_name ) )
	{
	  printf( "Failed to sync the mod '%s' to the server, cannot run\n", mod_name );
	  return -1;
	}
    }

  const char * game_path = get_var( &g_config.test_game );
  const char * remote_game_path = get_var( &g_config.remote_game_dir );
  const char * host_path = get_var( &g_config.remote_game_dir );
  const char * host_name = get_var( &g_config.remote_host );
  const char * local_port = get_var( &g_config.local_server_port );
  const char * remote_port = get_var( &g_config.remote_server_port );
  
  if( host_path == NULL || host_name == NULL || game_path == NULL || remote_game_path == NULL )
    {
      return -1;
    }
  
  //    args
  text_buffer_t args = {};
  {
    buffer__printf( &args, " +maxclients %d +gamedir %s", maxclients, mod_name );

    if( engine_id == ENGINE__FTE )
      {
	buffer__printf( &args, " +cl_splitscreen %d", screen_splits );
      }

    if( engine_id == ENGINE__LOCAL_CLIENT )
      {
	buffer__printf( &args, " +connect localhost:%s", local_port );
      }
    else if( engine_id == ENGINE__REMOTE_CLIENT )
      {
	buffer__printf( &args, " +connect %s:%s", host_name, remote_port );
      }
    else
      {
	buffer__printf( &args, " +map %s %s", map_name, get_gamemode_arg( gamemode_id ) );
      }

    if( engine_id == ENGINE__REMOTE_SERVER )
      {
	buffer__printf( &args, " -port %s", remote_port );
      }
    else if( engine_id == ENGINE__LOCAL_SERVER )
      {
	buffer__printf( &args, " -port %s", local_port );
      }
  }
  
  //    engine path and game path
  const char * engine_path = NULL;

  switch( engine_id )
    {
    default:
    case ENGINE__FTE:
      engine_path = get_var( &g_config.fte_path );
      break;

    case ENGINE__QUAKESPASM:
      engine_path = get_var( &g_config.qspasm_path );
      break;

    case ENGINE__LOCAL_SERVER:
      engine_path = get_var( &g_config.local_server_path );
      break;

    case ENGINE__REMOTE_SERVER:
      engine_path = get_var( &g_config.remote_server_path );
      break;
    }

  if( engine_path == NULL )
    {
      return -1;
    }
  
  //    command
  text_buffer_t command = {};
  switch( engine_id )
    {
    default:
      buffer__printf( &command, "%s %s", engine_path, args.text );
      break;

    case ENGINE__REMOTE_SERVER:
      buffer__printf( &command, "ssh %s \"cd %s && %s %s\"", host_name, remote_game_path, engine_path, args.text );
      break;      
    }
  
  chdir( game_path );

  buffer__clear( &args );

  print__message( LEVEL__NORMAL, "Running engine: %s\n", command.text );

  fflush( stdout );
  int ret = system( command.text );
  fflush( stdout );
  
  if( engine_id == ENGINE__REMOTE_SERVER )
    {
      text_buffer_t kill_command = {};
      buffer__printf( &kill_command, "ssh %s \"killall %s\"", host_name, engine_path );
      printf("Killing old server process\n");
      system( kill_command.text );
      printf("Done\n");
    }

  buffer__clear( &command );
  
  return ret;
}
