/*
  Copyright (C) 2018  Joshua D. Sawyer

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

//==============================================================================
//    Includes
//==============================================================================

//
//    System
//

#include <stdio.h>
#include <string.h>
char *strdup(const char *s);

//#define __READ_OUT_WALK

#ifdef __READ_OUT_WALK
#include <stdio.h>
#endif
#ifdef __COLLISION_WARNING
#include <stdio.h>
#endif

//
//    Engine
//

#include "array.h"

//==============================================================================
//    data array
//==============================================================================

void data__push( struct data_array * target, void * data )
{
  index_t index = target->count;
  target->count += 1;
  if( target->count >= target->alloc_count )
    {
      target->alloc_count += target->alloc_count + target->count + 10;
      target->entry = realloc( target->entry, target->entry_size * target->alloc_count );
    }

  memcpy( target->entry + index, data, target->entry_size );
}

void data__alloc_index( struct data_array * target, index_t index )
{
  index_t index_target = index + 1;
  if( index_target >= target->alloc_count )
    {
      target->alloc_count += target->alloc_count + index_target + 10;
      target->entry = realloc( target->entry, target->entry_size * target->alloc_count );
    }
}

//==============================================================================
//    voidp
//==============================================================================

#ifdef COMMON_ARRAY_VOIDP

void voidp__push( struct voidp_array * target, void * data )
{
  array__PUSH( *target, VOIDP_DATA( data ) );
}

void voidp__flip( struct voidp_array * target, index_t index, void * data )
{
  array__FLIP( *target, index, VOIDP_DATA( data ), (struct voidp_entry){} );
}

void voidp__shift( struct voidp_array * target, index_t index, void * data )
{
  array__SHIFT( *target, index, VOIDP_DATA( data ), (struct voidp_entry){} );
}

void voidp__set_index( struct voidp_array * target, index_t index, void * data, bool free_data )
{
  if( index >= array__COUNT( *target ) )
    {
      voidp__flip( target, index, data );
    }
  else
    {
      if( free_data )
	{
	  free_f free_f = (target->free != NULL) ? (target->free) : (free);
	  free_f( array__ENTRY( *target, index ).data );
	}

      array__ENTRY( *target, index ).data = data;
    }
}

void * voidp__pop( struct voidp_array * target, bool free_data )
{
  if( array__COUNT( *target ) == 0 )
    {
      return NULL;
    }
  
  array__IPUSH( *target );

  if( free_data )
    {
      free_f free_f = (target->free != NULL) ? (target->free) : (free);
      free_f( array__BOUND_ENTRY( *target ).data );
      return NULL;
    }
  else
    {
      return array__BOUND_ENTRY( *target ).data;
    }
}

void voidp__iflip( struct voidp_array * target, index_t index, bool free_data )
{
  if( free_data )
    {
      free_f free_f = (target->free != NULL) ? (target->free) : (free);
      free_f( array__ENTRY( *target, index ).data );
    }
  
  array__IFLIP( *target, index );
}

void voidp__ishift( struct voidp_array * target, index_t index, bool free_data )
{
  if( free_data )
    {
      free_f free_f = (target->free != NULL) ? (target->free) : (free);
      free_f( array__ENTRY( *target, index ).data );
    }

  array__ISHIFT( *target, index );
}

index_t voidp__find( struct voidp_array * target, void * data )
{
  index_t i;
  for( i = 0; i < array__COUNT( *target ); i++ )
    {
      if( array__ENTRY( *target, i ).data == data )
	{
	  return i;
	}
    }

  return INVALID_INDEX;
}

void voidp__clear( struct voidp_array * target, bool free_data )
{
  if( free_data )
    {
      free_f free_f = (target->free != NULL) ? (target->free) : (free);
      while( array__COUNT( *target ) > 0 )
	{
	  array__COUNT( *target ) -= 1;
	  free_f( array__BOUND_ENTRY( *target ).data );	  
	}
    }
  
  free( target->entry );
  *target = (struct voidp_array){};
}

#endif

//==============================================================================
//    Type
//==============================================================================

#ifdef COMMON_ARRAY_TYPE

void type__add( struct type_array * target, type_id_t id, free_f free )
{
  for( index_t i = 0; i < array__COUNT( *target ); i++ )
    {
      if( array__ENTRY( *target, i ).id == id )
	{
	  array__ENTRY( *target, i ).free = free;
	}
    }

  array__PUSH( *target, TYPE_ENTRY( id, free ) );
}

void type__free( struct type_array * target, type_id_t id, void * data )
{
  for( index_t i = 0; i < array__COUNT( *target ); i++ )
    {
      if( array__ENTRY( *target, i ).id == id )
	{
	  if( array__ENTRY( *target, i ).free != NULL )
	    {
	      array__ENTRY( *target, i ).free( data );
	    }
	  return;
	}
    }
}

void type__clear( struct type_array * target, bool free_data )
{
  free( target->entry );
  *target = (struct type_array){};
}

#endif

//==============================================================================
//    Typed array
//==============================================================================

#ifdef COMMON_ARRAY_ARTY

void arty__push( struct arty_array * target, void * data, size_t size, type_id_t type )
{
  array__PUSH( *target, ARTY_ENTRY( data, size, type ) );
}

void arty__flip( struct arty_array * target, index_t index, void * data, size_t size, type_id_t type )
{
  array__FLIP( *target, index, ARTY_ENTRY( data, size, type ), (struct arty_entry){} );
}

void arty__shift( struct arty_array * target, index_t index, void * data, size_t size, type_id_t type )
{
  array__SHIFT( *target, index, ARTY_ENTRY( data, size, type ), (struct arty_entry){} );
}

void arty__set_index( struct arty_array * target, index_t index, void * data, size_t size, type_id_t type, bool free_data )
{
  if( index >= array__COUNT( *target ) )
    {
      arty__flip( target, index, data, size, type );
    }
  else
    {
      if( free_data )
	{
	  type__free( &(target->type_array), array__ENTRY( *target, index ).type_id, array__ENTRY( *target, index ).data );
	}

      array__ENTRY( *target, index ) = ARTY_ENTRY( data, size, type );
    }
}

void arty__ipush( struct arty_array * target, bool free_data )
{
  array__IPUSH( *target );

  if( free_data )
    {
      type__free( &(target->type_array), array__BOUND_ENTRY( *target ).type_id, array__BOUND_ENTRY( *target ).data );
    }
}

void arty__iflip( struct arty_array * target, index_t index, bool free_data )
{
  if( free_data )
    {
      type__free( &(target->type_array), array__ENTRY( *target, index ).type_id, array__ENTRY( *target, index ).data );
    }
  
  array__IFLIP( *target, index );
}

void arty__ishift( struct arty_array * target, index_t index, bool free_data )
{
  if( free_data )
    {
      type__free( &(target->type_array), array__ENTRY( *target, index ).type_id, array__ENTRY( *target, index ).data );
    }

  array__ISHIFT( *target, index );
}

void arty__clear( struct arty_array * target, bool free_data )
{
  if( free_data )
    {
      struct type_array * type_array = &(target->type_array);
      struct type_entry * type_entry;
      index_t i;
      
      array__FLUSH( *type_array, type_entry, NULL_struct type_array )
	{
	  if( type_entry->free != NULL )
	    {
	      for( i = 0; i < array__COUNT( *target ); i++ )
		{
		  if( array__ENTRY( *target, i ).type_id == type_entry->id )
		    {
		      type_entry->free( array__ENTRY( *target, i ).data );
		    }
		}
	    }
	}
    }

  free( target->type_array.entry );
      
  free( target->entry );

  *target = (struct arty_array){};
}

#endif

//==============================================================================
//    hash
//==============================================================================

#ifdef COMMON_ARRAY_HASH

hash_t hash__calculate_hash( const char * str )
{
  hash_t hash = 65599;

  while( *str != '\0' )
    {
      hash = (*str) + (hash << 6) + (hash << 16) - hash;
      
      str += 1;
    }

  return hash;
}

static bool find_hash_order_index( struct hash_array * target, const char * name, hash_t hash, index_t * index )
{  
  (*index) = 0;

  index_t min = 0;
  index_t max = array__COUNT( *target ) - 1;

  while( (min <= max) && (max < array__COUNT( *target )) )
    {
      (*index) = (min + max) / 2;

      if( hash < array__ENTRY( *target, (*index) ).hash )
	{
	  max = (*index) - 1;
	}
      else if( hash > array__ENTRY( *target, (*index) ).hash )
	{
	  min = (*index) + 1;
	}
      else
	{
	  if( strcmp( name, array__ENTRY( *target, (*index) ).name ) == 0 )
	    {
	      return true;
	    }
	  else
	    {
#ifdef __COLLISION_WARNING
	      printf("collision, %u == %u, '%s' == '%s'\n",
		     hash,
		     array__ENTRY( *target, (*index) ).hash,
		     name,
		     array__ENTRY( *target, (*index) ).name );
#endif
	      
	      for( min = (*index);
		   ( (*index) < array__COUNT( *target ) ) && ( array__ENTRY(*target, (*index)).hash == hash );
		   (*index)-- )
		{
		  if( strcmp( name, array__ENTRY( *target, (*index) ).name ) == 0 )
		    {
		      return true;
		    }
		}
	      
	      for( (*index) = min;
		   ((*index) < array__COUNT( *target )) && (array__ENTRY( *target, (*index) ).hash == hash);
		   (*index)++ )
		{
		  if( strcmp( name, array__ENTRY( *target, (*index) ).name ) == 0 )
		    {
		      return true;
		    }
		}
	      return false;
	    }
	}
    }

  return false;
}

static bool find_index_order_index( struct hash_array * target, const char * name, hash_t hash, index_t * index )
{
  struct hash_entry * entry;

  array__FOR_EACH_ENTRY( *target, *index, entry )
    {
      if( (entry->hash == hash) && (strcmp( entry->name, name ) == 0) )
	{
	  //printf("%d/%d found\n", *index, array__COUNT( *target ) );
	  return true;
	}
      //printf("%d/%d fail\n", *index, array__COUNT( *target ) );
    }

  return false;
}
index_t hash__find_name( struct hash_array * target, const char * name )
{
  index_t ret;

  hash_t hash = hash__calculate_hash( name );

  if( !target->index_ordering )
    {
      if( find_hash_order_index( target, name, hash, &ret ) )
	{
	  return ret;
	}
      else
	{
	  return INVALID_INDEX;
	}
    }
  else
    {
      if( find_index_order_index( target, name, hash, &ret ) )
	{
	  return ret;
	}
      else
	{
	  printf("Fuck\n");
	  return INVALID_INDEX;
	}
    }
}

index_t hash__add_name( struct hash_array * target, const char * name )
{
  index_t index = 0;

  hash_t hash = hash__calculate_hash( name );

  if( !find_hash_order_index( target, name, hash, &index ) )
    {
      while( (index < array__COUNT( *target )) && (array__ENTRY( *target, index ).hash < hash) )
	{
	  index++;
	}
      
      struct hash_entry entry = HASH_ENTRY( strdup( name ), hash );
      
      array__ALLOC_NEXT( *target );
      
      memmove( target->entry + index + 1,
	       target->entry + index,
	       (array__COUNT( *target ) - index) * sizeof( array__FIRST_ENTRY( *target ) ) );
      array__COUNT( *target ) += 1;
      array__ENTRY( *target, index ) = entry;
    }
  
  return index;
}

index_t hash__push_name( struct hash_array * target, const char * name )
{
  target->index_ordering = true;
  
  index_t index = 0;

  hash_t hash = hash__calculate_hash( name );

  if( find_index_order_index( target, name, hash, &index ) )
    {
      return index;
    }
  else
    {
      array__PUSH( *target, HASH_ENTRY( strdup( name ), hash ) );
      return array__COUNT( *target ) - 1;
    }
}

void hash__clear( struct hash_array * target, bool free_data )
{
  if( free_data )
    {
      while( array__COUNT( *target ) > 0 )
	{
	  array__COUNT( *target ) -= 1;
	  free( array__BOUND_ENTRY( *target ).name );
	}
    }
  
  free( target->entry );
  *target = (struct hash_array){};
}

#endif

//==============================================================================
//    cache
//==============================================================================

#ifdef COMMON_ARRAY_CACHE

static bool find_cache_index( const struct cache_array * target, const char * name, hash_t hash, index_t * index )
{  
  (*index) = 0;
  
  index_t min = 0;
  index_t max = array__COUNT( *target ) - 1;

  while( (min <= max) && (max < array__COUNT( *target )) )
    {
      (*index) = (min + max) / 2;

      if( hash < array__ENTRY( *target, (*index) ).hash )
	{
	  max = (*index) - 1;
	}
      else if( hash > array__ENTRY( *target, (*index) ).hash )
	{
	  min = (*index) + 1;
	}
      else
	{
	  if( strcmp( name, array__ENTRY( *target, (*index) ).name ) == 0 )
	    {
	      return true;
	    }
	  else
	    {
#ifdef __COLLISION_WARNING
	      printf("collision, %u == %u, '%s' == '%s'\n",
		     hash,
		     array__ENTRY( *target, (*index) ).hash,
		     name,
		     array__ENTRY( *target, (*index) ).name );
#endif
	      
	      for( min = (*index);
		   ( (*index) < array__COUNT( *target ) ) && ( array__ENTRY( *target, (*index) ).hash == hash );
		   (*index)-- )
		{
		  if( strcmp( name, array__ENTRY( *target, (*index) ).name ) == 0 )
		    {
		      return true;
		    }
		}
	      
	      for( (*index) = min;
		   ( (*index) < array__COUNT( *target ) ) && ( array__ENTRY( *target, (*index) ).hash == hash );
		   (*index)++ )
		{
		  if( strcmp( name, array__ENTRY( *target, (*index) ).name ) == 0 )
		    {
		      return true;
		    }
		}
	      return false;
	    }
	}
    }

  return false;
}

void cache__add_type( struct cache_array * target, type_id_t id, free_f free )
{
  type__add( &(target->type_array), id, free );
}

index_t cache__find_index( const struct cache_array * target, const char * name )
{
  index_t ret;

  hash_t hash = hash__calculate_hash( name );

  if( find_cache_index( target, name, hash, &ret ) )
    {
      return ret;
    }
  else
    {
      return INVALID_INDEX;
    }
}

struct cache_entry * cache__find_entry( const struct cache_array * target, const char * name )
{
  index_t index = cache__find_index( target, name );
  if( index == INVALID_INDEX )
    {
      return NULL;
    }
  else
    {
      return &array__ENTRY( *target, index );
    }
}

void * cache__find_data( const struct cache_array * target, const char * name )
{
  index_t index = cache__find_index( target, name );
  if( index == INVALID_INDEX )
    {
      return NULL;
    }
  else
    {
      return array__ENTRY( *target, index ).data;
    }
}

index_t cache__add_name( struct cache_array * target, const char * name, void * data, size_t size, type_id_t type )
{
  index_t index = 0;

  hash_t hash = hash__calculate_hash( name );
  
  if( find_cache_index( target, name, hash, &index ) )
    {
      return index;
    }
  else
    {
      while( (index < array__COUNT( *target )) &&
	     (array__ENTRY( *target, index ).hash < hash) )
	{
	  index++;
	}
      
      array__ALLOC_NEXT( *target );
      
      memmove( target->entry + index + 1,
	       target->entry + index,
	       (array__COUNT( *target ) - index) * sizeof( array__FIRST_ENTRY( *target ) ) );
      array__COUNT( *target ) += 1;
      array__ENTRY( *target, index ) = CACHE_ENTRY( strdup( name ), hash, type, data, size );
    }

  return index;
}

void cache__delete_index( struct cache_array * target, index_t index, bool free_data )
{
  if( free_data )
    {
      type__free( &(target->type_array),
		  array__ENTRY( *target, index ).type_id,
		  array__ENTRY( *target, index ).data );
    }
  
  array__ISHIFT( *target, index );
}

void cache__delete_name( struct cache_array * target, const char * name, bool free_data )
{
  index_t index = cache__find_index( target, name );

  if( index != INVALID_INDEX )
    {
      cache__delete_index( target, index, free_data );
    }
}

void cache__clear( struct cache_array * target, bool free_data )
{
  if( free_data )
    {
      struct type_array * type_array = &(target->type_array);
      struct type_entry * type_entry;
      index_t i;
      
      array__FLUSH( *type_array, type_entry, NULL_struct type_array )
	{
	  if( type_entry->free != NULL )
	    {
	      for( i = 0; i < array__COUNT( *target ); i++ )
		{
		  if( array__ENTRY( *target, i ).type_id == type_entry->id )
		    {
		      type_entry->free( array__ENTRY( *target, i ).data );
		    }
		}
	    }
	}
      
      if( free_data )
	{
	  while( array__COUNT( *target ) > 0 )
	    {
	      array__COUNT( *target ) -= 1;
	      free( array__BOUND_ENTRY( *target ).name );
	    }
	}
    }

  free( target->type_array.entry );
      
  free( target->entry );

  *target = (struct cache_array){};
}

#endif

//==============================================================================
//    Array tree
//==============================================================================

#ifdef COMMON_ARRAY_TARR

static void fix_tarr_backreferences( struct tree_array * target, index_t start )
{
  index_t ip, ic;
  struct tree_array * parent;

  index_t end = array__COUNT( *target ) - 1;
  
  for( ip = start; ip <= end; ip++ )
    {
      parent = &array__ENTRY( *target, ip );
      parent->parent = target;

      for( ic = 0; ic < array__COUNT( *parent ); ic++ )
	{
	  array__ENTRY( *parent, ic ).parent = parent;
	}
    }
}

struct tree_array * tarr__push( struct tree_array * target, void * data, type_id_t type, type_id_t flags )
{
  struct tree_array * p = target->entry;

  array__PUSH( *target, INIT_tree_array( target, data, type, flags ) );

  //printf("second confirm flags %ld\n", array__LAST_ENTRY( *target ).flags);
  
  if( target->entry != p)
    {
      fix_tarr_backreferences( target, 0 );
    }
  
  return &array__LAST_ENTRY( *target );
}

/*void tarr__reparent_range( struct tree_array * orig_parent, struct tree_array * new_parent, index_t src_start, index_t dst_start, index_t count )
{
  array__ALLOC_INDEX( *new_parent, array__COUNT( *new_parent ) + count );
  
  memmove( &array__ENTRY( *new_parent, dst_start + count ), &array__ENTRY( *new_parent, dst_start ), (array__COUNT( *new_parent ) - dst_start - count - 1)*sizeof( array__ENTRY( *new_parent, 0 ) ) );

  memcpy( &array__ENTRY( *new_parent, dst_start ), &array__ENTRY( *orig_parent, src_start ), count*sizeof( array__ENTRY( *new_parent, 0 ) ) );

  array__COUNT( *new_parent ) += count;

  array__ISHIFT_RANGE( *orig_parent, src_start, count );

  fix_tarr_backreferences( orig_parent, src_start );

  fix_tarr_backreferences( new_parent, dst_start );
  }*/

struct tree_array * tarr__set_index( struct tree_array * target, index_t index, void * data, type_id_t type, type_id_t flags )
{
  if( index >= array__COUNT( *target ) )
    {
      struct tree_array * p = target->entry;
      						
      array__ALLOC_INDEX( *target, index );
      array__NULL_SPAN( *target, index, INIT_tree_array( target, NULL, 0, FL_Null ) );

      array__COUNT( *target ) = index + 1;
      
      if( target->entry != p)
	{
	  fix_tarr_backreferences( target, 0 );
	}
    }

  array__ENTRY( *target, index ) = INIT_tree_array( target, data, type, flags );

  return &array__ENTRY( *target, index );
}

void tarr__clear( struct tree_array * target, struct type_array * type_array, bool free_data );
void tarr__walk( struct tree_array * root, void * arg_data, tree_f op_func, enum tree_walk_flags walkfl )
{
  const int inc = (walkfl & WALKFL__REVERSE)?(-1):(1); // the increment direction
  
  struct voidp_array index_stack = (struct voidp_array){};

  struct tree_array * parent = root;
  struct tree_array * parent_parent = NULL;

  index_t child_index = (walkfl & WALKFL__REVERSE)?(array__COUNT( *root ) - 1):(0);
  
  // These goto's work like functions except they share the same stack space, so recursion is nice
  
  // Note that they all end with an explicit goto directive, so it shouldn't be too confusing to
  // figure out what's happening

 PARENT_START:
  // begin processing a parent's children
  {
#ifdef __READ_OUT_WALK
    printf( "%.*sStart %lu, %lu\n", (int)array__COUNT( index_stack ), "\t\t\t\t\t\t\t\t", array__COUNT( *parent ), child_index );
#endif

    struct tree_arg arg =
      {
	.root        = root,
	.target      = parent,
	.depth       = array__COUNT( index_stack ),
	.index       = child_index,
	.arg_data    = arg_data,
	.walk_target = WALK__PARENT_PRE,
      };

    if( !(walkfl & WALKFL__NO_PRE) )
      {
	switch( op_func( &arg ) )
	  {
	  case TREE__CONT:
	    break;
	  case TREE__NEXT_PEER:
	  case TREE__SKIP:
	    goto PARENT_END;
	    break;
	  case TREE__REPEAT:
	    goto PARENT_START;
	  case TREE__ISHIFT:
	    goto ISHIFT_PARENT;
	    break;
	  case TREE__PARENT:
	    goto PARENT_ASCEND;
	  case TREE__STOP:
	    goto QUIT;
	  }
      }
    
    goto PARENT_CONTINUE;
  }
  
 PARENT_CONTINUE:
  // continue to loop over a parent's children, either from the start or after ascending from a child
  {
    while( child_index < array__COUNT( *parent ) )
      {
#ifdef __READ_OUT_WALK
	printf("%.*sChild %lu/%lu\n", (int)array__COUNT( index_stack ), "\t\t\t\t\t\t\t\t", child_index, array__COUNT( *parent ) );
#endif	
	if( array__COUNT( array__ENTRY( *parent, child_index ) ) > 0 )
	  // if child has children
	  {
	    goto PARENT_DESCEND;
	  }
        
	struct tree_arg arg =
	  {
	    .root        = root,
	    .target      = &array__ENTRY( *parent, child_index ),
	    .depth       = array__COUNT( index_stack ) + 1,
	    .index       = child_index,
	    .arg_data    = arg_data,
	    .walk_target = WALK__CHILD,
	  };

	if( !(walkfl & WALKFL__NO_CHILD) )
	  {
	    switch( op_func( &arg ) )
	      {
	      case TREE__NEXT_PEER:
	      case TREE__CONT:
		break;
	      case TREE__SKIP:
		child_index = array__COUNT( *parent );
		break;
	      case TREE__ISHIFT:
		tarr__ishift( parent, child_index, NULL, false ); // then goto PARENT_CONTINUE
	      case TREE__REPEAT:
		goto PARENT_CONTINUE;
		break;
	      case TREE__PARENT:
		goto PARENT_ASCEND;
	      case TREE__STOP:
		goto QUIT;
	      }
	  }

	child_index += inc; // if inc is negative, this will loop around after 0 and exit
      }
    
    goto PARENT_END;
  }

 PARENT_DESCEND:
  // set the parent to a child and process the new set of children
  {
    parent_parent = parent;
    parent = &array__ENTRY( *parent_parent, child_index );
    voidp__push( &index_stack, (void*)child_index );
    child_index = (walkfl & WALKFL__REVERSE)?(array__COUNT( *parent ) - 1):(0);
    goto PARENT_START;
  }

 PARENT_ASCEND:
  // ascend back up to the previous parent after a child's children have been processed
  {
    parent = parent_parent;
    parent_parent = parent->parent;
    child_index = (index_t)voidp__pop( &index_stack, false ) + inc;
    goto PARENT_CONTINUE;
  }
  
 ISHIFT_PARENT:
  // ascend back up to the previous parent after a child's children have been processed
  {
    if( parent == root )
      {
	goto QUIT;
      }
    parent = parent_parent;
    parent_parent = parent->parent;
    child_index = (index_t)voidp__pop( &index_stack, false );
    tarr__ishift( parent, child_index, NULL, false );
    goto PARENT_CONTINUE;
  }
  
 PARENT_END:
  // finish processing a parent's children
  {

#ifdef __READ_OUT_WALK
    printf("%.*sEnd\n", (int)array__COUNT( index_stack ), "\t\t\t\t\t\t\t\t" );
#endif
    
    struct tree_arg arg =
      {
	.root        = root,
	.target      = parent,
	.depth       = array__COUNT( index_stack ),
	.index       = child_index,
	.arg_data    = arg_data,
	.walk_target = WALK__PARENT_POST,
      };
    
    if( !(walkfl & WALKFL__NO_POST) )
      {
	switch( op_func( &arg ) )
	  {
	  case TREE__NEXT_PEER:
	  case TREE__SKIP:
	  case TREE__CONT:
	    break;
	  case TREE__ISHIFT:
	    goto ISHIFT_PARENT;
	  case TREE__REPEAT:
	    goto PARENT_END;
	  case TREE__PARENT:
	    goto PARENT_ASCEND;
	  case TREE__STOP:
	    goto QUIT;
	  }
      }
    
    if( parent != root )
      {
	goto PARENT_ASCEND;
      }
    
    goto QUIT;
  }
  
 QUIT:
  // free up all that needs to be and exit
  {
    voidp__clear( &index_stack, false );
    return;
  }
}

struct tree_arg__free{
  struct type_array * type_array;
  bool free_data;
};

static enum tree_return free_data__walk_func( struct tree_arg * arg )
{
  if( arg->walk_target == WALK__PARENT_PRE )
    {
      return TREE__CONT;
    }
  
  struct tree_arg__free * arg_data = arg->arg_data;
  struct tree_array * target = arg->target;
  
  if( arg_data->free_data )
    {
      type__free( arg_data->type_array, target->type_id, target->data );
    }

  if( arg->walk_target == WALK__PARENT_POST )
    {
      free( target->entry );
      struct tree_array * parent = target->parent;
      *target = (struct tree_array){};
      target->parent = parent;
    }
  
  return TREE__CONT;
}
void tarr__clear( struct tree_array * target, struct type_array * type_array, bool free_data )
{
  tarr__walk( target,
	      &(struct tree_arg__free){ type_array, free_data },
	      free_data__walk_func,
	      false );
}

struct tree_array * tarr__pop( struct tree_array * target, struct type_array * type_array, bool free_data )
{
  tarr__clear( &array__LAST_ENTRY( *target ), type_array, free_data );
  
  array__IPUSH( *target );

  return &array__BOUND_ENTRY( *target );
}

void tarr__ishift( struct tree_array * target, index_t index, struct type_array * type_array, bool free_data )
{
  tarr__clear( &array__ENTRY( *target, index ), type_array, free_data );
  
  array__ISHIFT( *target, index );

  fix_tarr_backreferences( target, index );
}

void tarr__ishift_range( struct tree_array * target, index_t min, index_t max, struct type_array * type_array, bool free_data )
{
  if( free_data )
    {
      for( index_t i = min; i <= max; i++ )
	{
	  tarr__clear( &array__ENTRY( *target, i ), type_array, true );
	}
    }
  
  array__ISHIFT_RANGE( *target, min, max );

  fix_tarr_backreferences( target, min );
}

#endif

//==============================================================================
//    Single linked list 
//==============================================================================

#ifdef COMMON_ARRAY_LIST2

void list2__unlink( void * target_member_p, bool free_target_member )
{
  if( target_member_p == NULL )
    {
      return;
    }
  
  struct list2_member * target_member = target_member_p;
  
  struct list2 * target = target_member->parent;

  if( target == NULL )
    {
      goto ZERO;
    }

  struct list2_member * next = target_member->next;
  struct list2_member * previous = target_member->previous;

  // unlink
  
  if( next != NULL )
    {
      next->previous = previous;
    }

  if( previous != NULL )
    {
      previous->next = next;
    }

  if( target_member == target->first )
    {
      target->first = next;
    }

  if( target_member == target->last )
    {
      target->last = previous;
    }
  
  target->count -= 1;

 ZERO:
  
  if( free_target_member )
    {
      type__free( &(target->type_array), target_member->type_id, target_member );
    }
  else
    {
      target_member->next = target_member->previous = NULL;
      target_member->parent = NULL;
    }
}

void list2__clear( struct list2 * target, bool free_data )
{
  if( free_data )
    {
      struct list2_member * member;
      
      list2__FOR_EACH_LINK( *target, member )
	{
	  type__free( &(target->type_array), member->type_id, member );      
	}
    }
  
  voidp__clear( &target->parking, true );

  list2__unlink( target, false );
  
  *target = (struct list2){};
}

void list2__park( struct list2 * target, void * target_member_p )
{
  struct list2_member * target_member = target_member_p;

  list2__unlink( target_member, false );
  voidp__push( &target->parking, target_member );
}

void * list2__get_type( struct list2 * target, type_id_t type, size_t type_size )
{
  index_t i;
  struct list2_member * member;
  
  array__FOR_EACH_DATA( target->parking, i, member )
    {
      if( member->type_id == type )
	{
	  array__IFLIP( target->parking, i );
	  return member;
	}
    }

  return malloc( type_size );
}

#define list__ADD_FIRST( target, member )	\
  (target)->first = (target)->last = member;	\
  ((struct list2_member*)member)->parent = (target);	\
  (target)->count = 1;

void list2__append( struct list2 * target, type_id_t type, void * compatible_struct )
{
  if( target->count == 0 )
    {
      list__ADD_FIRST( target, compatible_struct );
    }
  else
    {
      list2__add_after( target, target->last, type, compatible_struct );
    }
}

void list2__prepend( struct list2 * target, type_id_t type, void * compatible_struct )
{
  if( target->count == 0 )
    {
      list__ADD_FIRST( target, compatible_struct );
    }
  else
    {
      list2__add_before( target, target->first, type, compatible_struct );
    }
}

void list2__add_before( struct list2 * target, void * target_member_p, type_id_t type, void * compatible_struct )
{
  struct list2_member * target_member = target_member_p;
  
  if( target_member == NULL )
    {
      if( target->count == 0 )
	{
	  list__ADD_FIRST( target, compatible_struct );
	}
      else
	{
	  list2__add_after( target, target->last, type, compatible_struct );
	}
      
      return;
    }
  
  struct list2_member * add = compatible_struct;
  struct list2_member * old_previous = target_member->previous;

  target_member->previous = add;
  
  if( old_previous != NULL )
    {
      old_previous->next = add;
    }
  else
    {
      target->first = add;
    }

  *add = (struct list2_member){ old_previous, target_member, target, type };
  
  target->count++;
}

void list2__add_after( struct list2 * target, void * target_member_p, type_id_t type, void * compatible_struct )
{
  struct list2_member * target_member = target_member_p;

  if( target_member == NULL )
    {
      if( target->count == 0 )
	{
	  list__ADD_FIRST( target, compatible_struct );
	  return;
	}
      else
	{
	  target_member = target->last;
	}
    }
  
  struct list2_member * add = compatible_struct;
  struct list2_member * old_next = target_member->next;

  target_member->next = add;
  
  if( old_next != NULL )
    {
      old_next->previous = add;
    }
  else
    {
      target->last = add;
    }

  *add = (struct list2_member){ target_member, old_next, target, type };
  
  target->count++;
}

#endif

//==============================================================================
