/*
  Copyright (C) 2018  Joshua D. Sawyer

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/*
    flags.h

    Enums of bitwise flags
*/
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef COMMON_FLAGS_H
#define COMMON_FLAGS_H

//==============================================================================
//    Type definitions
//==============================================================================

//
//    Null flag
//

#define FL_Null 0

//
//    8 bit flags
//

enum fl8
  {
    FL8_1 = 1 << 0,
    FL8_2 = 1 << 1,
    FL8_3 = 1 << 2,
    FL8_4 = 1 << 3,
    FL8_5 = 1 << 4,
    FL8_6 = 1 << 5,
    FL8_7 = 1 << 6,
    FL8_8 = 1 << 7,
  };

//
//    16 bit flags
//

enum fl16
  {
    FL16_1  = 1 << 0,
    FL16_2  = 1 << 1,
    FL16_3  = 1 << 2,
    FL16_4  = 1 << 3,
    FL16_5  = 1 << 4,
    FL16_6  = 1 << 5,
    FL16_7  = 1 << 6,
    FL16_8  = 1 << 7,
    FL16_9  = 1 << 8,
    FL16_10 = 1 << 9,
    FL16_11 = 1 << 10,
    FL16_12 = 1 << 11,
    FL16_13 = 1 << 12,
    FL16_14 = 1 << 13,
    FL16_15 = 1 << 14,
    FL16_16 = 1 << 15,
  };

enum fl32
  {
    FL32_1  = 1 << 0,
    FL32_2  = 1 << 1,
    FL32_3  = 1 << 2,
    FL32_4  = 1 << 3,
    FL32_5  = 1 << 4,
    FL32_6  = 1 << 5,
    FL32_7  = 1 << 6,
    FL32_8  = 1 << 7,
    FL32_9  = 1 << 8,
    FL32_10 = 1 << 9,
    FL32_11 = 1 << 10,
    FL32_12 = 1 << 11,
    FL32_13 = 1 << 12,
    FL32_14 = 1 << 13,
    FL32_15 = 1 << 14,
    FL32_16 = 1 << 15,
    FL32_17 = 1 << 16,
    FL32_18 = 1 << 17,
    FL32_19 = 1 << 18,
    FL32_20 = 1 << 19,
    FL32_21 = 1 << 20,
    FL32_22 = 1 << 21,
    FL32_23 = 1 << 22,
    FL32_24 = 1 << 23,
    FL32_25 = 1 << 24,
    FL32_26 = 1 << 25,
    FL32_27 = 1 << 26,
    FL32_28 = 1 << 27,
    FL32_29 = 1 << 28,
    FL32_30 = 1 << 29,
    FL32_31 = 1 << 30,
    FL32_32 = 1 << 31,
  };

enum fl64
  {
    FL64_1  = (unsigned long long) 1 << 0,
    FL64_2  = (unsigned long long) 1 << 1,
    FL64_3  = (unsigned long long) 1 << 2,
    FL64_4  = (unsigned long long) 1 << 3,
    FL64_5  = (unsigned long long) 1 << 4,
    FL64_6  = (unsigned long long) 1 << 5,
    FL64_7  = (unsigned long long) 1 << 6,
    FL64_8  = (unsigned long long) 1 << 7,
    FL64_9  = (unsigned long long) 1 << 8,
    FL64_10 = (unsigned long long) 1 << 9,
    FL64_11 = (unsigned long long) 1 << 10,
    FL64_12 = (unsigned long long) 1 << 11,
    FL64_13 = (unsigned long long) 1 << 12,
    FL64_14 = (unsigned long long) 1 << 13,
    FL64_15 = (unsigned long long) 1 << 14,
    FL64_16 = (unsigned long long) 1 << 15,
    FL64_17 = (unsigned long long) 1 << 16,
    FL64_18 = (unsigned long long) 1 << 17,
    FL64_19 = (unsigned long long) 1 << 18,
    FL64_20 = (unsigned long long) 1 << 19,
    FL64_21 = (unsigned long long) 1 << 20,
    FL64_22 = (unsigned long long) 1 << 21,
    FL64_23 = (unsigned long long) 1 << 22,
    FL64_24 = (unsigned long long) 1 << 23,
    FL64_25 = (unsigned long long) 1 << 24,
    FL64_26 = (unsigned long long) 1 << 25,
    FL64_27 = (unsigned long long) 1 << 26,
    FL64_28 = (unsigned long long) 1 << 27,
    FL64_29 = (unsigned long long) 1 << 28,
    FL64_30 = (unsigned long long) 1 << 29,
    FL64_31 = (unsigned long long) 1 << 30,
    FL64_32 = (unsigned long long) 1 << 31,
    FL64_33 = (unsigned long long) 1 << 32,
    FL64_34 = (unsigned long long) 1 << 33,
    FL64_35 = (unsigned long long) 1 << 34,
    FL64_36 = (unsigned long long) 1 << 35,
    FL64_37 = (unsigned long long) 1 << 36,
    FL64_38 = (unsigned long long) 1 << 37,
    FL64_39 = (unsigned long long) 1 << 38,
    FL64_40 = (unsigned long long) 1 << 39,
    FL64_41 = (unsigned long long) 1 << 40,
    FL64_42 = (unsigned long long) 1 << 41,
    FL64_43 = (unsigned long long) 1 << 42,
    FL64_44 = (unsigned long long) 1 << 43,
    FL64_45 = (unsigned long long) 1 << 44,
    FL64_46 = (unsigned long long) 1 << 45,
    FL64_47 = (unsigned long long) 1 << 46,
    FL64_48 = (unsigned long long) 1 << 47,
    FL64_49 = (unsigned long long) 1 << 48,
    FL64_50 = (unsigned long long) 1 << 49,
    FL64_51 = (unsigned long long) 1 << 50,
    FL64_52 = (unsigned long long) 1 << 51,
    FL64_53 = (unsigned long long) 1 << 52,
    FL64_54 = (unsigned long long) 1 << 53,
    FL64_55 = (unsigned long long) 1 << 54,
    FL64_56 = (unsigned long long) 1 << 55,
    FL64_57 = (unsigned long long) 1 << 56,
    FL64_58 = (unsigned long long) 1 << 57,
    FL64_59 = (unsigned long long) 1 << 58,
    FL64_60 = (unsigned long long) 1 << 59,
    FL64_61 = (unsigned long long) 1 << 60,
    FL64_62 = (unsigned long long) 1 << 61,
    FL64_63 = (unsigned long long) 1 << 62,
    FL64_64 = (unsigned long long) 1 << 63,
  };

//
//    Generic flags
//

enum fl
  { 
    FL_1  = FL8_1,
    FL_2  = FL8_2,
    FL_3  = FL8_3,
    FL_4  = FL8_4,
    FL_5  = FL8_5,
    FL_6  = FL8_6,
    FL_7  = FL8_7,
    FL_8  = FL8_8,
    FL_9  = FL16_9,
    FL_10 = FL16_10,
    FL_11 = FL16_11,
    FL_12 = FL16_12,
    FL_13 = FL16_13,
    FL_14 = FL16_14,
    FL_15 = FL16_15,
    FL_16 = FL16_16,
    FL_17 = FL32_17,
    FL_18 = FL32_18,
    FL_19 = FL32_19,
    FL_20 = FL32_20,
    FL_21 = FL32_21,
    FL_22 = FL32_22,
    FL_23 = FL32_23,
    FL_24 = FL32_24,
    FL_25 = FL32_25,
    FL_26 = FL32_26,
    FL_27 = FL32_27,
    FL_28 = FL32_28,
    FL_29 = FL32_29,
    FL_30 = FL32_30,
    FL_31 = FL32_31,
    FL_32 = FL32_32,
    FL_33 = FL64_33,
    FL_34 = FL64_34,
    FL_35 = FL64_35,
    FL_36 = FL64_36,
    FL_37 = FL64_37,
    FL_38 = FL64_38,
    FL_39 = FL64_39,
    FL_40 = FL64_40,
    FL_41 = FL64_41,
    FL_42 = FL64_42,
    FL_43 = FL64_43,
    FL_44 = FL64_44,
    FL_45 = FL64_45,
    FL_46 = FL64_46,
    FL_47 = FL64_47,
    FL_48 = FL64_48,
    FL_49 = FL64_49,
    FL_50 = FL64_50,
    FL_51 = FL64_51,
    FL_52 = FL64_52,
    FL_53 = FL64_53,
    FL_54 = FL64_54,
    FL_55 = FL64_55,
    FL_56 = FL64_56,
    FL_57 = FL64_57,
    FL_58 = FL64_58,
    FL_59 = FL64_59,
    FL_60 = FL64_60,
    FL_61 = FL64_61,
    FL_62 = FL64_62,
    FL_63 = FL64_63,
  };

//==============================================================================

#endif // ifdef COMMON_FLAGS_H
