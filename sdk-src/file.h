/*
  Copyright (C) 2018  Joshua D. Sawyer

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef FILE_H
#define FILE_H

#include "array.h"
#include "text.h"
#include "defaults.h"

#include <stdbool.h>
#include <dirent.h>
#include <time.h>

void file__list_dir( struct voidp_array * dir_list, const char * dir_name, const char * fuzzy_filter, size_t fuzzy_ignore, const char * extension, bool get_dirs );
void file__list_subdirs( struct voidp_array * list, const char * dir, const char * fuzzy_filter, size_t fuzzy_ignore, const char * extension, bool get_dirs );
//void file__list_var_subdirs( struct voidp_array * list, struct config_var * var, const char * fuzzy_filter, const char * extension, bool get_dirs );
void file__recursive_find_dirs( struct voidp_array * dir_list, const char * dir, const char * fuzzy_filter );
void file__recursive_find_files( struct voidp_array * file_list, const char * dir, const char * fuzzy_filter, const char * extension );
time_t file__mod_time( const char * file_path );
bool file__src_is_newer( const char * src, const char * dst );
bool file__src_list_is_newer( struct voidp_array * src_list, const char * dst );
bool file__src_list_dst_list_compare( struct voidp_array * src_list, struct voidp_array * dst_list );

int file__recursive_mkdir( const char * target );

bool file__exists( const char * path );
bool file__is_dir( const char * path );
bool file__is_file( const char * path );
void file__remove( const char * path );
int file__sync_remote( const char * local_path, const char * host, const char * remote_path );

#endif // #ifndef FILE_H
