/*
  Copyright (C) 2018  Joshua D. Sawyer

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef DEFAULTS_H
#define DEFAULTS_H

#include "array.h"
#include "string.h"
#include "text.h"

struct config_var
{
  char * name;
  struct config_var * parent;
  char * default_val;
  char * value;
};

struct config
{
  bool is_set;
  struct config_var user_home,
    dir,
    bin,
    default_mod,
    default_map,
    qcc_path,
    qbsp_path,
    light_path,
    light_flags,
    qbsp_flags,
    qcc_flags,
    maps_dir,
    mods_dir,
    fte_path,
    qspasm_path,
    map_editor_path,
    text_editor_path,

    remote_game_dir,
    local_server_port,
    remote_server_port,
    local_server_path,
    remote_server_path,
    remote_host,
    
    test_game;
};

extern struct config g_config;

void assign_default_config( struct config * config );
const char * get_var( struct config_var * var );

#endif // #ifndef DEFAULTS_H
