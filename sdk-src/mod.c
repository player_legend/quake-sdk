/*
  Copyright (C) 2018  Joshua D. Sawyer

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "file.h"
#include "text.h"
#include "defaults.h"
#include "print.h"
#include "mod.h"

void mod__find( struct voidp_array * list, const char * fuzzy_filter )
{
  const char * mods_dir = get_var( &g_config.mods_dir );

  if( mods_dir == NULL )
    {
      print__message( LEVEL__ERROR, "Couldn't search for mods.\n");
    }

  file__list_subdirs( list, mods_dir, fuzzy_filter, strlen( mods_dir ), NULL, true );
}

bool mod__has_makefile( const char * mod_source )
{
  char * makefile_path = text__new( mod_source );
  text__pathcat( &makefile_path, "Makefile" );
  bool ret = file__exists( makefile_path );
  free( makefile_path );
  return ret;
}

char * mod__get_target_name( struct voidp_array * mod_list )
{
  return text__get_target_name( mod_list, &g_config.default_mod );
}

char * mod__get_mod_dest( const char * mod )
{
  const char * game_dir = get_var( &g_config.test_game );
  
  if( game_dir == NULL )
    {
      return NULL;
    }
  
  char * mod_name = text__get_name( mod );
  
  char * mod_dest;
  {
    mod_dest = text__new( game_dir );
    text__pathcat( &mod_dest, mod_name );
  }

  free( mod_name );

  return mod_dest;
}

int mod__edit( const char * mod_dir )
{
  const char * qc_extension = ".qc";
  struct voidp_array qc_list = {};

  file__recursive_find_files( &qc_list, mod_dir, NULL, qc_extension );

  if( array__COUNT( qc_list ) == 0 )
    {
      print__message( LEVEL__NORMAL, "No .qc files to edit in '%s'\n", mod_dir );
    }
  
  const char * text_editor = get_var( &g_config.text_editor_path );

  if( text_editor == NULL )
    {
      print__message( LEVEL__ERROR, "Couldn't launch the text editor\n" );
    }
  
  char * command = text__new( text_editor );

  index_t file_index;
  const char * file;

  array__FOR_EACH( data, qc_list, file_index, file )
    {
      text__wordcat( &command, file );
    }

  int ret = system( command );

  free( command );

  return ret;
}

/*

static void sub_get_qc_deps( struct hash_array * deps_table, text_buffer_t buffer )
{
  const char * str = buffer.text;
  const char * str_end;
  const char * include_string = "#include";

  while( (str = strstr( str, include_string )) != NULL )
    {
      str += strlen( include_string );
      while( 1 )
	{
	  if( *str != ' ' &&
	      *str != '\t' &&
	      *str != '<' &&
	      *str != '"' )
	    {
	      break;
	    }
	}

      str_end = str;

      while( 1 )
	{
	  if( *str_end == ' ' ||
	      *str_end == '\t' ||
	      *str_end == '\n' ||
	      *str_end == '>' ||
	      *str_end == '"'||
	      *str_end == '\0' )
	    {
	      break;
	    }
	}

      if( str_end > str )
	{
	  size_t len = str_end - str + 1;
	  char add[len];
	  strncpy( add, str, len );
	  hash__add_name( deps_table, add );
	}
    }
}

static void sub_get_src_deps( struct hash_array * deps_table, text_buffer_t buffer )
{
  const char * str = buffer.text;
  const char * str_end;

  do
    {
      str_end = strchr( str, '\n' );

      if( str_end == NULL )
	{
	  str_end = str + strlen( str );
	}

      if( str_end > str )
	{
	  size_t len = str_end - str + 1;
	  char add[len];
	  strncpy( add, str, len );
	  hash__add_name( deps_table, add );
	}
    }
  while( str != NULL && *str != '\0' );
}

static void recursive_get_deps( struct voidp_array * output, const char * src_file )
{
  voidp__push( output, text__new( src_file ) );
  
  #warning todo
}
*/

bool mod__compile_qc( const char * mod_dir )
{
  const char * qc_extension = ".qc";
  const char * qh_extension = ".qh";
  const char * src_extension = ".src";
  const char * dat_extension = ".dat";

  const char * qcc_path = get_var( &g_config.qcc_path );
  const char * qcc_flags = get_var( &g_config.qcc_flags );
  char * mod_dest = mod__get_mod_dest( mod_dir );

  if( qcc_path == NULL || qcc_flags == NULL || mod_dest == NULL )
    {
      print__message( LEVEL__ERROR, "Couldn't get QCC path, failed to compile '%s'\n", mod_dir );
      free( mod_dest );
      return false;
    }

  struct voidp_array src_list = {};
  struct voidp_array change_files_list = {};
  struct voidp_array target_files_list = {};
    
  {
    file__recursive_find_files( &change_files_list, mod_dir, NULL, src_extension );
    file__recursive_find_files( &change_files_list, mod_dir, NULL, qc_extension );
    file__recursive_find_files( &change_files_list, mod_dir, NULL, qh_extension );
    
    file__recursive_find_files( &target_files_list, mod_dest, NULL, dat_extension );
    
    file__recursive_find_files( &src_list, mod_dir, NULL, src_extension );
  }

  if( !file__src_list_dst_list_compare( &change_files_list, &target_files_list ) )
    {
      char * mod_name = text__get_name( mod_dir );

      print__message( LEVEL__NORMAL, "'%s' is up to date\n", mod_name );
      
      free( mod_name );
      
      voidp__clear( &src_list, true );
      voidp__clear( &change_files_list, true );
      voidp__clear( &target_files_list, true );

      return true;
    }

  index_t src_index;
  const char * src_path;
  
  char * dst_name;
  char * dst_path;
  char * command;

  char * command_start = text__new( qcc_path );
  text__wordcat( &command_start, qcc_flags );
  
  file__recursive_mkdir( mod_dest );

  array__FOR_EACH( data, src_list, src_index, src_path )
    {
      dst_name = text__get_name( src_path );
      dst_path = text__path2( mod_dest, dst_name, dat_extension );
      
      command = text__new( command_start );
      text__wordcat( &command, src_path );
      text__wordcat( &command, "-o" );
      text__wordcat( &command, dst_path );
      
      printf("\n================================================================================\n");
      printf("\tCompile %s.src\n", dst_name);
      printf("\tCommand %s\n", command);
      printf("================================================================================\n\n");

      fflush( stdout );
      
      if( system( command ) != 0 )
	{
	  printf("\n");
	  goto FAIL;
	}
      
      free( dst_name );
      free( dst_path );
      free( command );
    }

  free( command_start );
  free( mod_dest );
  return true;
  
 FAIL:
  free( command_start );
  free( mod_dest );
  return false;
}

index_t mod__compile_list( struct voidp_array * mod_list )
{
  index_t mod_index;
  const char * mod_source;

  index_t compile_count = 0;
  
  array__FOR_EACH( data, *mod_list, mod_index, mod_source )
    {
      if( mod__compile_qc( mod_source ) )
	{
	  compile_count += 1;
	}
      else
	{
	  char * name = text__get_name( mod_source );
	  print__message( LEVEL__ERROR, "Compiling mod '%s' failed\n", name );
	  free( name );
	  break;
	}
    }

  return compile_count;
}

void mod__clear( const char * mod )
{
  char * mod_dest = mod__get_mod_dest( mod );

  file__remove( mod_dest );

  free( mod_dest );
}

void mod__clear_list( struct voidp_array * mod_list )
{
  index_t index;
  const char * name;

  print__message( LEVEL__NORMAL, "Clearing %u mods\n", (unsigned int)array__COUNT( *mod_list ) );
  print__startsection();
  array__FOR_EACH( data, *mod_list, index, name )
    {
      mod__clear( name );
    }
  print__endsection();
  printf("\n");
}

void mod__print_path_list( struct voidp_array * list )
{
  index_t index;
  const char * path;

  const char * mods_dir = get_var( &g_config.mods_dir );

  char * cut;

  print__startsection();

  array__FOR_EACH( data, *list, index, path )
    {
      cut = text__cut_dir( path, mods_dir );

      print__message( LEVEL__NORMAL, "%s\n", cut );
      
      free( cut );
    }
  
  print__endsection();
  printf("\n");
}

bool mod__sync( const char * mod_name )
{
  const char * local_game_dir = get_var( &g_config.test_game );
  const char * remote_game_dir = get_var( &g_config.remote_game_dir );
  const char * remote_host = get_var( &g_config.remote_host );

  if( local_game_dir == NULL || remote_game_dir == NULL || remote_host == NULL )
    {
      return false;
    }

  char * local_mod_dir = text__path2( local_game_dir, mod_name, NULL );
  char * remote_mod_dir = text__path2( remote_game_dir, mod_name, NULL );

  int ret = file__sync_remote( local_mod_dir, remote_host, remote_mod_dir );

  if( ret == 0 )
    {
      ret = true;
    }
  else
    {
      ret = false;
    }

  free( local_mod_dir );
  free( remote_mod_dir );
  
  return ret;
}
